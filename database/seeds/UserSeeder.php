<?php

use Illuminate\Database\Seeder;
use  Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => Str::random(10),
            'family' => Str::random(10),
            'phone' => Str::random(10),
            'state' => Str::random(10),
            'city' => Str::random(10),
            'sex' => Str::random(10),
            'national_id' => Str::random(10),
            'password' => Str::random(10),
            'type' => 'student',
            'image' => Str::random(10),
        ]);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => Str::random(10),
            'family' => Str::random(10),
            'phone' => Str::random(10),
            'state' => Str::random(10),
            'city' => Str::random(10),
            'sex' => Str::random(10),
            'national_id' => Str::random(10),
            'password' => Str::random(10),
            'type' => 'student',
            'image' => Str::random(10),
        ]);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => Str::random(10),
            'family' => Str::random(10),
            'phone' => Str::random(10),
            'state' => Str::random(10),
            'city' => Str::random(10),
            'sex' => Str::random(10),
            'national_id' => Str::random(10),
            'password' => Str::random(10),
            'type' => 'student',
            'image' => Str::random(10),
        ]);

    }
}
