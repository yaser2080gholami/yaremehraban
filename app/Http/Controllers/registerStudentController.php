<?php

namespace App\Http\Controllers;

use App\Admin\ConfirmationCode;
use App\Admin\Course;
use App\Admin\school;
use App\Admin\student;
use App\Http\Requests\StudentRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class registerStudentController extends Controller
{
    public function index()
    {
//        if (Auth::check()){
//            return redirect('dashboard');
//
//        }
        $schools = school::all();
        $courses = Course::all();
        return view('registerStudent', compact(['schools', 'courses']));
    }

    /**
     * @param Request $request
     */
    public function postinfo(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'family' => 'required',
            'state' => 'required',
            'city' => 'required',
            'sex' => 'required',
            'course_id' => 'required',
            'phone' => 'required',
            'national_id' => 'required',
            'password' => 'required|confirmed',
        ]);
        $request->session()->flash('phone',$request->get('phone'));
        $request->session()->flash('national_id',$request->get('national_id'));
        DB::transaction(function () use ($request) {
            $user = new User();
            $user->name = $request->get('name');
            $user->family = $request->get('family');
            $user->state = $request->get('state');
            $user->city = $request->get('city');
            $user->sex = $request->get('sex');
            $user->phone = $request->get('phone');
            $user->national_id = $request->get('national_id');
            $user->password = bcrypt($request->get('password'));
            $user->type = 'student';
            $user->image = $request->get('image') ?? null;
            $user->save();
            student::create([
                'student_id' => $user->id,
                'course_id' => $request->get('course_id'),
                'school_id' => $request->get('school'),
            ]);
            $code = str_pad(rand(0, pow(10, 6) - 1), 6, '0', STR_PAD_LEFT);
            $confirm_code = new ConfirmationCode();
            $confirm_code->code = $code;
            $confirm_code->user_id = $user->id;
            $confirm_code->save();
        });
        return redirect(route('verify'));
    }

    public function verify(Request $request)
    {
        if (! $request->session()->has('phone')){
            return redirect(route('registerStudent'));
        }
        $request->session()->reflash();
        return view('VerifyCode');
    }

    public function postverify(Request $request)
    {
        $request->validate([
            'token' => 'required'
        ]);
        $now = Carbon::now()->addMinutes(-2);
        $user = User::where('national_id', $request->session()->get('national_id'))->first();
        $confirm_code = ConfirmationCode::where('user_id', $user->id)->where('is_used', 0)->where('created_at', '>', $now)->first();
        if ($confirm_code->code == $request->token) {
            DB::transaction(function () use ($confirm_code) {
                $user = User::find(Auth::id());
                $user->is_active = 1;
                $user->save();
                $confirm_code->is_used = 1;
                $confirm_code->save();
                Auth::login($user, true);
            });
            return redirect('dashboard');
        }
    }

    public function resendCode(Request $request)
    {
        $code = str_pad(rand(0, pow(10, 6) - 1), 6, '0', STR_PAD_LEFT);
        $user = Auth::user();
//        if ($user->is_active){
//            return redirect('dashboard');
//        }
        DB::transaction(function () use ($code, $user) {
            ConfirmationCode::where('user_id', $user->id)->update([
                'is_used' => 1
            ]);
            $confirm_code = new ConfirmationCode();
            $confirm_code->code = $code;
            $confirm_code->user_id = $user->id;
            $confirm_code->save();
        });
        return redirect(route('verify'));
    }
}
