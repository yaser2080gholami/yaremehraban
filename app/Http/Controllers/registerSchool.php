<?php

namespace App\Http\Controllers;

use App\Admin\period;
use App\Admin\PeriodSchool;
use App\Admin\school;
use App\User;
use Illuminate\Http\Request;

class registerSchool extends Controller
{
    public function index()
    {
        $periods = period::all();
        return view('registerSchool',compact('periods'));
  }

    public function infoSchool(Request $request)
    {
        $data =$request->validate([
            'name' => 'required',
            'family' => 'required',
            'state' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'description' => 'required',
            'password' => 'required|confirmed',
            'schoolEductionLevelList' => 'required'
        ]);
        $user = new User();
        $user->name = $request->get('name');
        $user->family = $request->get('family');
        $user->phone = $request->get('phone');
        $user->national_id = $request->get('national_id');
        $user->type ='manager';
        $user->state = $request->get('state');
        $user->city = $request->get('city');
        $user->password = $request->get('password');
        $user->save();

        $school=school::create([
            'manager_id' => $user->id,
            'name' => $request->get('schoolName'),
            'phone_number' => $request->get('schoolPhoneNumber'),
            'identity' => mt_rand(10000,100000),
            'description' => $request->get('description'),
            'address' => $request->get('address'),
            'province' => $request->get('state'),
            'city' => $request->get('city')
        ]);
//        $PeriodSchool = new PeriodSchool();
//        $PeriodSchool->school_id = $school->id;
//        $PeriodSchool->period_id = $request->get('schoolEductionLevelList');
//        $PeriodSchool->save();
        $school->period()->sync($data['schoolEductionLevelList']);
        return redirect(route('dashboard'));
  }
}
