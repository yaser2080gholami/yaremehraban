<?php

namespace App\Http\Controllers\ManagerPanelController;

use App\Admin\Course;
use App\Admin\school;
use App\Admin\SchoolClass;
use App\Admin\SchoolClassStudent;
use App\Admin\student;
use App\Admin\Supervisor;
use App\Admin\teachers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        $schools = school::where('manager_id', Auth::id())->first();
        $students = [];
        $courses = Course::all();
        $students = student::where('school_id', $schools->id)->with('course', 'user', 'parent')->get();
        return view('panel_manager.CreateClass.DefineStudent', compact(['students', 'courses']));
    }

    public function create(Request $request)
    {
//        $this->authorize('see-class');
        $request->validate([
            'name' => 'required',
            'family' => 'required',
            'phone' => 'required',
            'national_id' => 'required',
            'father_name' => 'required',
            'course_id' => 'required',
        ]);
        $password = str_pad(rand(0, pow(10, 6) - 1), 6, '0', STR_PAD_LEFT);
        DB::transaction(function () use ($password, $request) {
            $user = new User();
            $user->name = $request->get('name');
            $user->family = $request->get('family');
            $user->phone = $request->get('phone');
            $user->national_id = $request->get('national_id');
            $user->password = bcrypt($password);
            $user->is_active = 1;
            $user->type = 'student';
            $user->save();
            if ($request->get('no_father')) {
                $parent = new User();
                $parent->name = $request->get('parent_name');
                $parent->family = $request->get('parent_family');
                if ($request->get('same_number')) {
                    $parent->phone = $request->get('phone');
                } else {
                    $parent->phone = $request->get('parent_phone');
                }
                $parent->national_id = $request->get('parent_national_id');
                $parent->password = bcrypt($password);
                $parent->type = 'parent';
                $parent->is_active = 1;
                $parent->save();
                $relative = $request->get('relative');
            } else {
                $parent = new User();
                $parent->name = $request->get('father_name');
                $parent->family = $request->get('family');
                if ($request->get('same_number')) {
                    $parent->phone = $request->get('phone');
                } else {
                    $parent->phone = $request->get('parent_phone');
                }
                $parent->national_id = $request->get('parent_national_id');
                $parent->password = bcrypt($password);
                $parent->type = 'parent';
                $parent->is_active = 1;
                $parent->save();
                $relative = 'پدر';
            }
            $school = school::where('manager_id', Auth::id())->first();
            student::create([
                'student_id' => $user->id,
                'school_id' => $school->id,
                'parent_id' => $parent->id,
                'course_id' => $request->get('course_id'),
                'relative' => $relative,
                'same_number' => $request->get('same_number'),
            ]);
        });
        return redirect(route('manager.student.index'));
    }


    public function delete(Request $request, $id)
    {
//        $this->authorize('see-class');
        student::where('student_id', $id)->update([
            'school_id' => null
        ]);
        SchoolClassStudent::where('student_id', $id)->update([
            'student_id' => null
        ]);

        return redirect(route('manager.student.index'));
    }

    public function update(Request $request)
    {
//        $this->authorize('see-class');
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'family' => 'required',
            'phone' => 'required',
            'national_id' => 'required',
            'father_name' => 'required',
            'course_id' => 'required',
            'parent_national_id' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            $user = User::find($request->get('id'));$user->name = $request->get('name');
            $user->family = $request->get('family');
            $user->phone = $request->get('phone');
            $user->national_id = $request->get('national_id');
            $user->save();
            $same_number = $request->get('same_number') ?? '';
            $no_father = $request->get('no_father') ?? '';
            if ($no_father !== ''){
                student::where('student_id', $request->get('id'))->update([
                    'relative' => 'پدر'
                ]);
                $parent = User::where('national_id', $request->get('parent_national_id'))->first();
                $parent->name = $request->get('parent_name');
                $parent->family = $request->get('parent_family');
                if ($same_number !== '') {
                    $parent->phone = $request->get('phone');
                    student::where('student_id', $request->get('id'))->update([
                        'same_number' => 1
                    ]);
                } else {
                    $parent->phone = $request->get('parent_phone');
                    student::where('student_id', $request->get('id'))->update([
                        'same_number' => 0
                    ]);
                }
                $parent->national_id = $request->get('parent_national_id');
                $parent->save();
            } else {
                student::where('student_id', $request->get('id'))->update([
                    'relative' => $request->get('relative') ?? ''
                ]);
                $parent = User::where('national_id', $request->get('parent_national_id'))->first();
                $parent->name = $request->get('father_name');
                $parent->family = $request->get('family');
                if ($request->get('same_number')) {
                    $parent->phone = $request->get('phone');
                    student::where('student_id', $request->get('id'))->update([
                        'same_number' => 1
                    ]);
                } else {
                    $parent->phone = $request->get('parent_phone');
                    student::where('student_id', $request->get('id'))->update([
                        'same_number' => 0
                    ]);
                }
                $parent->national_id = $request->get('parent_national_id');
                $parent->save();
            }
        });

        return redirect(route('manager.student.index'));
    }

    public function activate(Request $request, $id)
    {
        $this->authorize('see-class');
        $user = User::find($id);
        $password = str_pad(rand(0, pow(10, 6) - 1), 6, '0', STR_PAD_LEFT);
        $url = "https://ippanel.com/services.jspd";
        $param = array
        (
            'from' => '+983000505',
            'uname'=>'reseller09196809561',
            'pass'=>'Sdg45jko98jaz',
            'message'=>$password,
            'to'=>'09126522137',
        );
        $url = " http://ippanel.com/class/sms/webservice/send_url.php?from=+983000505&to=09126522137&msg=sss&uname=reseller09196809561&pass=Sdg45jko98jaz";
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handler, CURLOPT_POSTFIELDS, $param);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $response2 = curl_exec($handler);
        $user->password = bcrypt($password);
        $user->save();
        return redirect(route('manager.student.index'));
    }
}
