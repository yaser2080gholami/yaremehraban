<?php

namespace App\Http\Controllers\ManagerPanelController;

use App\Admin\ConfirmationCode;
use App\Admin\school;
use App\Admin\SchoolClass;
use App\Admin\student;
use App\Admin\teachers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TeacherController extends Controller
{
    public function index(Request $request)
    {
//        $this->authorize('see-class');
        $schools = school::where('manager_id', Auth::id())->pluck('id');
        $teachers = teachers::whereIn('school_id', $schools)->with('user')->get();
        return view('panel_manager.CreateClass.index', compact('teachers'));
    }

    public function create(Request $request)
    {
//        $this->authorize('see-class');
        $request->validate([
            'name' => 'required',
            'family' => 'required',
            'phone' => 'required',
            'national_id' => 'required',
            'father_name' => 'required',
            'sheba' => 'required',
        ]);
        $password = str_pad(rand(0, pow(10, 6) - 1), 6, '0', STR_PAD_LEFT);
        DB::transaction(function () use ($password, $request) {
            $user = new User();
            $user->name = $request->get('name');
            $user->family = $request->get('family');
            $user->phone = $request->get('phone');
            $user->national_id = $request->get('national_id');
            $user->password = bcrypt($password);
            $user->type = 'teacher';
            $user->save();
            $school = school::where('manager_id', Auth::id())->first();
            teachers::create([
                'teacher_id' => $user->id,
                'school_id' => $school->id,
                'sheba_number' => $request->get('sheba'),
                'father_name' => $request->get('father_name'),
            ]);
        });
        return redirect(route('manager.teacher.index'));
    }



    public function delete(Request $request, $id)
    {
//        $this->authorize('see-class');
        $school = school::where('manager_id', Auth::id())->first();
        if (!SchoolClass::where('teacher_id', $id)->where('school_id', $school->id)->first()) {
            teachers::where('teacher_id', $id)->update([
                'school_id' => null
            ]);
            return redirect(route('manager.teacher.index'));
        } else {
            return back()->withErrors('نمیتوان معلمی را که کلاس دارد، حذف نمود.');
        }
    }

    public function update(Request $request)
    {
//        $this->authorize('see-class');
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'family' => 'required',
            'phone' => 'required',
            'national_id' => 'required',
            'father_name' => 'required',
            'sheba' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            $user = User::find($request->get('id'));
            $user->name = $request->get('name');
            $user->family = $request->get('family');
            $user->phone = $request->get('phone');
            $user->national_id = $request->get('national_id');
            $user->save();
            teachers::where('teacher_id', $request->get('id'))->update([
                'father_name' => $request->get('father_name'),
                'sheba_number' => $request->get('sheba')
            ]);
        });

        return redirect(route('manager.teacher.index'));
    }

}
