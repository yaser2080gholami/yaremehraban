<?php

namespace App\Http\Controllers\ManagerPanelController;

use App\Admin\personal;
use App\Admin\school;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PersonalController extends Controller
{
    public function index()
    {
//        $this->authorize('see-class');
        $personals = personal::with('user')->get();
        return view('panel_manager.CreateClass.DefinePersonnel', compact('personals'));
    }

    public function create(Request $request)
    {
//        $this->authorize('see-class');
        $request->validate([
            'name' => 'required',
            'family' => 'required',
            'father_name' => 'required',
            'job_title' => 'required',
            'sheba' => 'required'
        ]);
        $user = new User();
        $user->name = $request->get('name');
        $user->family = $request->get('family');
        $user->phone = $request->get('phone');
        $user->national_id = $request->get('national_id');
        $user->password = bcrypt(Str::random(40));
        $user->type = 'personal';
        $user->save();
        $school = school::where('manager_id', Auth::id())->first();
        personal::create([
            'personal_id' => $user->id,
            'school_id' => $school->id,
            'father_name' => $request->get('father_name'),
            'job_title' => $request->get('job_title'),
            'sheba' => $request->get('sheba')
        ]);
        return redirect(route('manager.personal.index'));
    }

    public function update(Request $request)
    {
//        $this->authorize('see-class');
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'family' => 'required',
            'father_name' => 'required',
            'national_id' => 'required',
            'phone' => 'required',
            'job_title' => 'required',
            'sheba' => 'required'
        ]);
        $user = User::find($request->get('id'));
        $user->name = $request->get('name');
        $user->family = $request->get('family');
        $user->phone = $request->get('phone');
        $user->save();
        personal::where('personal_id',$request->get('id'))->update([
            'father_name' => $request->get('father_name'),
            'job_title' => $request->get('job_title'),
            'sheba' => $request->get('sheba'),
        ]);
        return redirect(route('manager.personal.index'));
    }

    public function delete(Request $request , $id)
    {
//        $this->authorize('see-class');
        $school = school::where('manager_id', Auth::id())->first();
        personal::where('personal_id',$id)->update([
            'school_id' => null
        ]);
        personal::where('personal_id',$id)->delete();
        return redirect(route('manager.personal.index'));
    }

}
