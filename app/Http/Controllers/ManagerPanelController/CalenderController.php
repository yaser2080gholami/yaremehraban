<?php

namespace App\Http\Controllers\ManagerPanelController;

use App\Admin\Course;
use App\Admin\Schedule;
use App\Admin\school;
use App\Admin\SchoolClass;
use App\Admin\SchoolClassStudent;
use App\Admin\teachers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class CalenderController extends Controller
{

    public function index()
    {}

    public function create(Request $request)
    {
//        $this->authorize('see-class');
        $this->validate($request, [
            'day' => 'required',
            'start_time' => 'required',
            'duration' => 'required',
            'label' => 'required',
            'class_id' => 'required',
        ]);
        $schedule = new Schedule();
        $schedule->label = $request->get('label');
        $schedule->day = $request->get('day');
        $schedule->start_time = $request->get('start_time');
        $schedule->duration = $request->get('duration');
        $schedule->class_id = $request->get('class_id');
        $schedule->save();
    }

}
