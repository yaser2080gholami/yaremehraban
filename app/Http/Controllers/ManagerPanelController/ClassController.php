<?php

namespace App\Http\Controllers\ManagerPanelController;

use App\Admin\Course;
use App\Admin\school;
use App\Admin\SchoolClass;
use App\Admin\SchoolClassStudent;
use App\Admin\teachers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ClassController extends Controller
{
    public function index()
    {
//        $this->authorize('see-class');
        $classes = SchoolClass::where('school_id', Auth::user()->school[0]->id)->with('teacher', 'course', 'student')->get();
        $courses = Course::all();
        $school = school::where('manager_id', Auth::id())->first();
        $teachers = teachers::where('school_id', $school->id)->with('user')->get();
        return view('panel_manager.CreateClass.DefineClass', compact(['classes', 'courses', 'teachers']));
    }

    public function addStudent(Request $request)
    {
//        $this->authorize('see-class');
        $user = User::where('national_id', $request->get('national_id'))->first();
        SchoolClassStudent::create([
            'student_id' => $user->id,
            'school_class_id' => $request->get('class_id')
        ]);
        return $user;
    }

    public function showStudent($id)
    {
    }

    public function create(Request $request)
    {
//        $this->authorize('see-class');
        $this->validate($request, [
            'capacity' => 'required',
            'class_name' => 'required',
            'teacher_id' => 'required',
            'course_id' => 'required',
        ]);
        $school = school::where('manager_id', Auth::id())->first();
        $class = new SchoolClass();
        $class->label = $request->get('class_name');
        $class->capacity = $request->get('capacity');
        $class->teacher_id = $request->get('teacher_id');
        $class->course_id = $request->get('course_id');
        $class->school_id = $school->id;
        $class->save();
        return redirect(route('manager.class.index'));
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);
        SchoolClass::where('id', $request->get('id'))->update([
            'school_id' => null
        ]);
        return redirect(route('manager.class.index'));
    }
}
