<?php

namespace App\Http\Controllers\ManagerPanelController;

use App\Admin\Course;
use App\Admin\school;
use App\Admin\SchoolClass;
use App\Admin\SchoolClassStudent;
use App\Admin\task;
use App\Admin\task_answer;
use App\Admin\teachers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function index()
    {
        $this->authorize('see-class');
        $school = school::where('manager_id', Auth::id())->first();
        $classes = SchoolClass::where('school_id', $school->id)->pluck('id');
        $tasks = task::whereIn('class_id', $classes)->with(['school_class.course', 'teacher.user'])->get();
        return view('panel_manager.task', compact(['tasks']));
    }

    public function answers(Request $request)
    {
        $this->authorize('see-class');
        $this->validate($request, [
            'id' => 'required'
        ]);
        $task = task::find($request->get('id'));
        $class = SchoolClass::find($task->class_id);
        $answers = task_answer::where('task_id', $request->get('id'))->with(['student.user'])->get();
        return view('panel_manager.task_answers', compact(['class', 'answers', 'task']));
    }

    public function downloadFile(Request $request)
    {
//        $this->authorize('task.add');
        $this->validate($request, [
            'id' => 'required'
        ]);
        $answer = task_answer::find($request->get('id'));
        $file = public_path() . "/upload/answers/".$answer->file_name;

        $headers = [
            'Content-Type: application/pdf',
            "Content-type: image/jpeg"
        ];

        return response()->download($file, $answer->file_name, $headers);;
    }

}
