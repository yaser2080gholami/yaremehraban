<?php

namespace App\Http\Controllers;

use App\Admin\SchoolClass;
use App\Admin\SchoolClassStudent;
use App\Admin\student;
use App\Admin\teachers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AjaxController extends Controller
{
    public function getTicketData(Request $request){
        $this->validate($request, [
            'state' => 'required'
        ]);
        $user = User::where('id', Auth::id())->with('school')->first();
        if ($request->input('state') === 'teacher'){

            if ($user->type === 'manager'){
                return teachers::where('school_id', $user->school[0]->id)->with('user')->get();


            } elseif ($user->type === 'student'){
                $classes = SchoolClassStudent::where('student_id', $user->id)->pluck('school_class_id');
                $teachers = SchoolClass::whereIn('id', $classes)->pluck('teacher_id');
                return teachers::whereIn('teacher_id', $teachers)->with('user')->get();


            } elseif ($user->type === 'parent') {
                $students = student::where('parent_id', $user->id)->pluck('student_id');
                $classes = SchoolClassStudent::whereIn('student_id', $students)->pluck('school_class_id');
                $teachers = SchoolClass::whereIn('id', $classes)->pluck('teacher_id');
                return teachers::whereIn('teacher_id', $teachers)->with('user')->get();


            }
        } elseif ($request->input('state') === 'student') {
            if ($user->type === 'manager'){
                return SchoolClass::where('school_id', $user->school[0]->id)->get();


            } elseif ($user->type === 'student'){
                $classes = SchoolClassStudent::where('student_id', $user->id)->pluck('school_class_id');
                $students = SchoolClassStudent::whereIn('school_class_id', $classes)->pluck('student_id');
                return student::whereIn('student_id', $students)->with('user')->get();


            } elseif ($user->type === 'teacher') {
                return SchoolClass::where('teacher_id', $user->id)->get();


            }
        } elseif ($request->input('state') === 'parent') {
            if ($user->type = 'manager'){
                return SchoolClass::where('school_id', $user->school[0]->id)->get();


            } elseif ($user->type === 'teacher') {
                return SchoolClass::where('teacher_id', $user->id)->get();
            }
        }
    }

    public function getStudentsData(Request $request){
        $this->validate($request, [
            'class_id' => 'required'
        ]);
        return SchoolClass::where('id', $request->get('class_id'))->with('student')->get();
    }
}
