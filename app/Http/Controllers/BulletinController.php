<?php

namespace App\Http\Controllers;

use App\Admin\Bulletin;
use App\Admin\school;
use App\Admin\SchoolBulletin;
use App\Admin\student;
use App\Admin\teachers;
use App\Admin\YareMehrabanBulletin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BulletinController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if ($user->type === 'manager') {
            $school = school::where('manager_id', $user->id)->first();
            $school_id = $school->id;
        } elseif ($user->type === 'teacher') {
            $school = teachers::where('teacher_id', $user->id)->first();
            $school_id = $school->school_id;
        } elseif ($user->type === 'student') {
            $school = student::where('student_id', $user->id)->first();
            $school_id = $school->school_id;
        } elseif ($user->type === 'parent'){
            $schools = student::where('parent_id', Auth::id())->pluck('school_id');
            $school_bulletins = SchoolBulletin::whereIn('school_id', $schools)->where('is_active', 1)->get();
            $yare_mehraban_bulletins = YareMehrabanBulletin::where('is_active', 1)->get();
            return view('panel_parents.dashboard', compact(['school_bulletins', 'yare_mehraban_bulletins']));
        }
        $res = Bulletin::where('school_id', $school_id)->get();
        $bulletin = [];
        foreach ($res as $row) {
            $bulletin[$row->key] = $row->value;
        }
        $school = school::find($school_id);
        $school_bulletins = SchoolBulletin::where('school_id', $school_id)->where('is_active', 1)->get();
        $yare_mehraban_bulletins = YareMehrabanBulletin::where('is_active', 1)->get();
        return view('panel_student.bulletin', compact(['bulletin', 'school_bulletins', 'yare_mehraban_bulletins', 'school']));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'key' => 'required',
            'value' => 'required'
        ]);
        if ($request->get('key') === 'school_bulletin') {
            $user = Auth::user();
            $school = school::where('manager_id', $user->id)->first();
            $bulletin = new SchoolBulletin();
            $bulletin->school_id = $school->id;
            $bulletin->content = $request->get('value');
            $bulletin->save();
        } elseif ($request->get('key') === 'yare_mehraban_bulletin') {
            $bulletin = new YareMehrabanBulletin();
            $bulletin->content = $request->get('value');
            $bulletin->save();
        } else {
            $user = Auth::user();
            $school = school::where('manager_id', $user->id)->first();
            $bulletin = Bulletin::where('school_id', $school->id)->where('key', $request->get('key'))->first();
            if (!$bulletin) {
                $bulletin = new Bulletin();
                $bulletin->school_id = $school->id;
                $bulletin->key = $request->get('key');
            }
            $bulletin->value = $request->get('value');
            $bulletin->save();
        }
        return redirect('bulletin');
    }

    public function edit(Request $request)
    {
        $this->authorize('bulletin-edit');
        return view('bulletin.insert');
    }

    public function deleteSchool(Request $request, $id){
        $bulletin = SchoolBulletin::find($id);
        $bulletin->is_active = 0;
        $bulletin->save();
        return redirect('bulletin');
    }

    public function deleteMehraban(Request $request, $id){
        $bulletin = YareMehrabanBulletin::find($id);
        $bulletin->is_active = 0;
        $bulletin->save();
        return redirect('bulletin');
    }

}
