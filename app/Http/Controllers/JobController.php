<?php

namespace App\Http\Controllers;

use App\Admin\job;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    public function postjob(Request $request)
    {
        $request->validate([
            'comment' => 'required',
            'deadline' => 'required'
        ]);
        if (empty($request->get('id'))) {
            job::create([
                'user_id' => $request->user()->id,
                'comment' => $request->get('comment'),
                'deadline' => $request->get('deadline')
            ]);
        } else {
            $job = job::find($request->get('id'));
            $job->comment = $request->get('comment');
            $job->deadline = $request->get('deadline');
            $job->save();
        }
       return back();
    }

    public function deletejob($id)
    {
       $job = job::findOrfail($id);
       $job->delete();
       return back();
    }
}
