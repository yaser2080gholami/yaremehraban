<?php

namespace App\Http\Controllers;

use App\Admin\Bulletin;
use App\Admin\job;
use App\Admin\school;
use App\Admin\SchoolBulletin;
use App\Admin\SchoolClass;
use App\Admin\student;
use App\Admin\teachers;
use App\Admin\YareMehrabanBulletin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashbordController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if ($user->type === 'manager') {
            $school = school::where('manager_id', $user->id)->first();
            if (isset($school->id)) {
                $school_id = $school->id;
            }
        } elseif ($user->type === 'teacher') {
            $school = teachers::where('teacher_id', $user->id)->first();
            if (isset($school->school_id)){
                $school_id = $school->school_id;
            }
        } elseif ($user->type === 'student') {
            $school = student::where('student_id', $user->id)->first();
            if (isset($school->school_id)) {
                $school_id = $school->school_id;
            }
        } elseif ($user->type === 'parent'){
            $schools = student::where('parent_id', Auth::id())->pluck('school_id');
            $school_bulletins = SchoolBulletin::whereIn('school_id', $schools)->where('is_active', 1)->get();
            $yare_mehraban_bulletins = YareMehrabanBulletin::where('is_active', 1)->get();
            return view('panel_parents.dashboard', compact(['school_bulletins', 'yare_mehraban_bulletins']));
        }
        $bulletin = [];
        $school_bulletins = [];
        if (isset($school_id)){
            $res = Bulletin::where('school_id', $school_id)->get();
            $school = school::find($school_id);
            foreach ($res as $row) {
                $bulletin[$row->key] = $row->value;
            }
            $school_bulletins = SchoolBulletin::where('school_id', $school_id)->where('is_active', 1)->get();
        }
        $yare_mehraban_bulletins = YareMehrabanBulletin::where('is_active', 1)->get();
        $jobs = job::where('user_id', Auth::id())->paginate(3);
        return view('panel_student.dashboard', compact(['jobs' ,'bulletin', 'school_bulletins', 'yare_mehraban_bulletins', 'school']));
    }
}
