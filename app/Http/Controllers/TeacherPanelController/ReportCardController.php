<?php

namespace App\Http\Controllers\TeacherPanelController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportCardController extends Controller
{
    public function index(Request $request)
    {
//        $this->authorize('ReportCard.read');
        return view('panel_teacher\report_card');
    }
}
