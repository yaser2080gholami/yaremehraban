<?php

namespace App\Http\Controllers\TeacherPanelController;

use App\Admin\DescriptiveQuestion;
use App\Admin\TestQuestion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index()
    {
//        $this->authorize('question.read');
    }

    public function create()
    {
//        $this->authorize('question.add');
    }

    public function store(Request $request)
    {
//        $this->authorize('question.add');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
//        $this->authorize('question.edit');
    }

    public function update(Request $request, $id)
    {
//        $this->authorize('question.edit');
    }

    public function destroy($id)
    {
//        $this->authorize('question.delete');
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);
        if ($request->get('type') === 'test') {
            $this->validate($request, [
                'homework_id' => 'required',
                'option1' => 'required',
                'option2' => 'required',
                'option4' => 'required',
                'option3' => 'required',
                'correct_option' => 'required',
                'title' => 'required',
            ]);
            $question = new TestQuestion();
            $question->homework_id = $request->get('homework_id');
            $question->option1 = $request->get('option1');
            $question->option2 = $request->get('option2');
            $question->option3 = $request->get('option3');
            $question->option4 = $request->get('option4');
            $question->correct_option = $request->get('correct_option');
            $question->title = $request->get('title');
            $question->save();
        } else {
            $this->validate($request, [
                'homework_id' => 'required',
                'grade' => 'required',
                'title' => 'required',
            ]);
            $question = new DescriptiveQuestion();
            $question->homework_id = $request->get('homework_id');
            $question->title = $request->get('title');
            $question->grade = $request->get('grade');
            $question->save();
        }
        return redirect(route('teacher.homework.index'));
    }
}
