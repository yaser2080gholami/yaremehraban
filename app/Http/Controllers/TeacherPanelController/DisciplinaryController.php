<?php

namespace App\Http\Controllers\TeacherPanelController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DisciplinaryController extends Controller
{
    public function index(Request $request)
    {
//        $this->authorize('Disciplinary.read');
        return view('panel_teacher\disciplinary');
    }

    public function create(Request $request)
    {
//        $this->authorize('Disciplinary.add');
        return view('panel_teacher\disciplinary');
    }

    public function delete(Request $request)
    {
//        $this->authorize('Disciplinary.delete');
        return view('panel_teacher\disciplinary');
    }
}
