<?php

namespace App\Http\Controllers\TeacherPanelController;

use App\Admin\OnlineClass;
use App\Admin\SchoolClass;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OnlineClassController extends Controller
{
    public function index(Request $request)
    {
        $classes = SchoolClass::whereNotNull('school_id')->where('teacher_id', Auth::id())->get();
        return view('panel_teacher.online_class', compact(['classes']));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'class_id' => 'required',
            'link' => 'required',
        ]);
        $online = new OnlineClass();
        $online->class_id = $request->get('class_id');
        $online->url = $request->get('link');
        $online->save();
        return redirect(route('teacher.online.index'));
    }

}
