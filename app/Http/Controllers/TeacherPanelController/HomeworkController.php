<?php

namespace App\Http\Controllers\TeacherPanelController;

use App\Admin\Course;
use App\Admin\homework;
use App\Admin\SchoolClass;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeworkController extends Controller
{
    public function index()
    {
//        $this->authorize('Homework.read');
        $homeworks = homework::where('teacher_id', Auth::id())->with(['course'])->get();
        $classes = SchoolClass::where('teacher_id', Auth::id())->pluck('course_id');
        $courses = Course::whereIn('id', $classes)->get();
        return view('panel_teacher.Homework.homework', compact(['courses', 'homeworks']));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'lesson_name' => 'required',
            'title' => 'required',
            'course_id' => 'required',
            'question_number' => 'required',
            'expire' => 'required',
            'type' => 'required',
            'start_date_date' => 'required',
            'start_date_hour' => 'required',
        ]);
        $start_date = $request->get('start_date_date').' '.$request->get('start_date_hour');
        $homework = new homework();
        $homework->lesson_name = $request->get('lesson_name');
        $homework->title = $request->get('title');
        $homework->course_id = $request->get('course_id');
        $homework->question_number = $request->get('question_number');
        $homework->expire = $request->get('expire');
        $homework->type = $request->get('type');
        $homework->start_date = $start_date;
        $homework->teacher_id = Auth::id();
        $homework->save();
        return redirect(route('teacher.homework.index'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'lesson_name' => 'required',
            'title' => 'required',
            'course_id' => 'required',
            'question_number' => 'required',
            'expire' => 'required',
            'type' => 'required',
            'start_date_date' => 'required',
            'start_date_hour' => 'required',
        ]);
        $start_date = $request->get('start_date_date').' '.$request->get('start_date_hour');
        $homework = homework::find($request->get('id'));
        $homework->lesson_name = $request->get('lesson_name');
        $homework->title = $request->get('title');
        $homework->course_id = $request->get('course_id');
        $homework->question_number = $request->get('question_number');
        $homework->expire = $request->get('expire');
        $homework->type = $request->get('type');
        $homework->start_date = $start_date;
        $homework->teacher_id = Auth::id();
        $homework->save();
        return redirect(route('teacher.homework.index'));
    }

    public function delete(Request $request)
    {
//        $this->authorize('Homework.delete');
        $this->validate($request, [
            'id' => 'required',
        ]);
        $homework = homework::find($request->get('id'));
        $homework->delete();
        return redirect(route('teacher.homework.index'));
    }
}
