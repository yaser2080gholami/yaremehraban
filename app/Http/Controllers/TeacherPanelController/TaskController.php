<?php

namespace App\Http\Controllers\TeacherPanelController;

use App\Admin\SchoolClass;
use App\Admin\task;
use App\Admin\task_answer;
use App\Admin\teachers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    public function index(Request $request)
    {
//        $this->authorize('teacher.read');
        $teacher = teachers::where('teacher_id', Auth::user()->id)->with('school_class.course')->first();
        $task = task::where('teacher_id', Auth::user()->id)->with('school_class.course')->get();
        return view('panel_teacher\task', compact(['teacher', 'task']));
    }

    public function create(Request $request)
    {
//        $this->authorize('task.add');
        $this->validate($request, [
            'lesson_name' => 'string|required',
            'title' => 'string|required',
            'course_id' => 'integer|required',
            'deadline-date' => 'string|required',
            'deadline-hour' => 'string|required',
            'class_id' => 'integer|required',
            'content' => 'string|required',
        ]);
        $deadline = $request->get('deadline-date') . ' ' . $request->get('deadline-hour');
        task::create([
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'class_id' => $request->get('class_id'),
            'lesson_name' => $request->get('lesson_name'),
            'deadline' => $deadline,
            'teacher_id' => Auth::user()->id ?? 1,
        ]);
        return redirect(route('teacher.task.index'));
    }

    public function update(Request $request)
    {
//        $this->authorize('task.edit');
        $this->validate($request, [
            'id' => 'integer|required',
            'lesson_name' => 'string|required',
            'title' => 'string|required',
            'course_id' => 'integer|required',
            'deadline-date' => 'string|required',
            'deadline-hour' => 'string|required',
            'class_id' => 'integer|required',
            'content' => 'string|required',
        ]);
        $deadline = $request->get('deadline-date') . ' ' . $request->get('deadline-hour');
        $task = task::find($request->get('id'));
        $task->title = $request->get('title');
        $task->content = $request->get('content');
        $task->class_id = $request->get('class_id');
        $task->lesson_name = $request->get('lesson_name');
        $task->deadline = $deadline;
        $task->teacher_id = $request->get('teacher_id');
        $task->save();
        return redirect(route('teacher.task.index'));
    }

    public function correctTask(Request $request)
    {
//        $this->authorize('tas.edit');
        $this->validate($request, [
            'id' => 'required|integer',
            'comment' => 'required|string',
            'grade' => 'required',
        ]);
        $answer = task_answer::find($request->get('id'));
        $answer->comment_teacher = $request->get('comment');
        $answer->grade = $request->get('grade');
        $answer->save();
        return redirect(route('teacher.task.correct-index'));
    }

    public function delete(Request $request)
    {
//        $this->authorize('teacher.read');
        $this->validate($request, [
            'id' => 'integer|required'
        ]);
        DB::transaction(function () use ($request) {
            $answers = task_answer::where('task_id', $request->get('id'))->get();
            foreach ($answers as $answer) {
                $answer->delete();
            }
            $task = task::find($request->get('id'));
            $task->delete();
        });
        return redirect(route('teacher.task.index'));
    }

    public function correctTaskIndex(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);
        $task = task::find($request->get('id'));
        $class = SchoolClass::find($task->class_id);
        $answers = task_answer::where('task_id', $request->get('id'))->with('student.user')->get();
        return view('panel_teacher.task_answers', compact(['class', 'answers', 'task']));
    }

    public function downloadFile(Request $request)
    {
//        $this->authorize('task.add');
        $this->validate($request, [
            'id' => 'required'
        ]);
        $answer = task_answer::find($request->get('id'));
        $file = public_path() . "/upload/answers/".$answer->file_name;

        $headers = [
            'Content-Type: application/pdf',
            "Content-type: image/jpeg"
        ];

        return response()->download($file, $answer->file_name, $headers);;
    }
}
