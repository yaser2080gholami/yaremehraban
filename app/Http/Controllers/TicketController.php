<?php

namespace App\Http\Controllers;

use App\Admin\school;
use App\Admin\SchoolClass;
use App\Admin\student;
use App\Admin\teachers;
use App\Admin\ticket;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    public function index(Request $request)
    {
        $tickets = ticket::where(function ($q) {
            $q->where('user_id', Auth::id())
                ->orWhere('destination_id', Auth::id());
        })->whereNull('ticket_id')->with(['user', 'destination', 'grandchildren'])->get();
        return view('panel_teacher.ticket.list_ticket', compact(['tickets']));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'priority' => 'required',
            'subject' => 'required',
            'destination' => 'required',
            'text' => 'required',
        ]);
        $ticket = new ticket();
        $ticket->priority = $request->get('priority');
        $ticket->subject = $request->get('subject');
        $ticket->comment = $request->get('text');
        $ticket->user_id = Auth::id();
        if ($request->get('destination') === 'manager') {
            if (Auth::user()->type === 'teacher') {
                $teacher = teachers::whereNotNull('school_id')->where('teacher_id', Auth::id())->first();
                $school = school::find($teacher->school_id);
                $ticket->destination_id = $school->manager_id;
            } elseif (Auth::user()->type === 'student') {
                $student = student::whereNotNull('school_id')->where('student_id', Auth::id())->first();
                $school = school::find($student->school_id);
                $ticket->destination_id = $school->manager_id;
            } elseif (Auth::user()->type === 'parent') {
                $student = student::whereNotNull('school_id')->where('parent_id', Auth::id())->first();
                $school = school::find($student->school_id);
                $ticket->destination_id = $school->manager_id;
            }
        } else {
            $this->validate($request, [
                'user_id' => 'required',
            ]);
            $ticket->destination_id = $request->get('user_id');
        }
        if ($request->hasFile('file')) {
            $fileName = time() . '.' . $request->file->extension();
            $request->file->move(public_path('upload/tickets'), $fileName);
            $ticket->file_name = $fileName;
        }
        $ticket->save();
        return view('panel_teacher.ticket.list_ticket');

    }

    public function sendTicket(Request $request)
    {
        //todo creating ticket staff
        return view('panel_teacher.ticket.ticket');
    }
}
