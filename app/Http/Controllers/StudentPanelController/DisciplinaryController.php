<?php

namespace App\Http\Controllers\StudentPanelController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DisciplinaryController extends Controller
{
    public function index(Request $request)
    {
//        $this->authorize('Disciplinary.read');
        return view('panel_student\disciplinary');
    }

}
