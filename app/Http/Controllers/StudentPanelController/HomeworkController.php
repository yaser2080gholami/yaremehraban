<?php

namespace App\Http\Controllers\StudentPanelController;

use App\Admin\homework;
use App\Admin\SchoolClass;
use App\Admin\SchoolClassStudent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeworkController extends Controller
{
    public function indexDescriptive(Request $request)
    {
//        $this->authorize('Homework.read');
        $classes = SchoolClassStudent::where('student_id', Auth::id())->whereNotNull('school_class_id')->pluck('school_class_id');
        $teachers = SchoolClass::whereIn('id', $classes)->whereNotNull('school_id')->whereNotNull('teacher_id')->pluck('teacher_id');
        $courses = SchoolClass::whereIn('id', $classes)->whereNotNull('school_id')->whereNotNull('teacher_id')->pluck('course_id');
        $homeworks = homework::whereIn('teacher_id', $teachers)->whereIn('course_id', $courses)->where('type', 'تشریحی')->with(['descriptive_question'])->get();
        return view('panel_student.Homework.DescriptiveHomework', compact(['homeworks']));
    }

    public function answerDescriptive(Request $request)
    {
//        $this->authorize('Homework.read');
        return view('panel_student.Homework.DescriptiveHomework');
    }

    public function indexTest(Request $request)
    {
//        $this->authorize('Homework.read');
        $classes = SchoolClassStudent::where('student_id', Auth::id())->whereNotNull('school_class_id')->pluck('school_class_id');
        $teachers = SchoolClass::whereIn('id', $classes)->whereNotNull('school_id')->whereNotNull('teacher_id')->pluck('teacher_id');
        $courses = SchoolClass::whereIn('id', $classes)->whereNotNull('school_id')->whereNotNull('teacher_id')->pluck('course_id');
        $homeworks = homework::whereIn('teacher_id', $teachers)->whereIn('course_id', $courses)->where('type', 'تستی')->with(['test_question'])->get();
        return view('panel_student.Homework.testhomework', compact(['homeworks']));
    }

    public function answerTest(Request $request)
    {
//        $this->authorize('Homework.read');
        return view('panel_student.Homework.testhomework');
    }

}
