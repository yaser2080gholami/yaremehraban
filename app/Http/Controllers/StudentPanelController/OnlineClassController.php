<?php

namespace App\Http\Controllers\StudentPanelController;

use App\Admin\OnlineClass;
use App\Admin\SchoolClassStudent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OnlineClassController extends Controller
{
    public function index(Request $request)
    {
        $student_classes = SchoolClassStudent::where('student_id', Auth::id())->pluck('school_class_id');
        $classes = OnlineClass::whereIn('class_id', $student_classes)->with(['class.teacher.user'])->get();
        return view('panel_student.online_class', compact(['classes']));
    }

}
