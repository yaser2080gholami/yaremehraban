<?php

namespace App\Http\Controllers\StudentPanelController;

use App\Admin\SchoolClass;
use App\Admin\student;
use App\Admin\task;
use App\Admin\task_answer;
use App\Admin\teachers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function index(Request $request)
    {
//        $this->authorize('task.read');
        $student = student::where('student_id', Auth::id())->with('classes')->first();
        $classes = [];
        foreach ($student->classes as $class) {
            $classes[] = $class->id;
        }
        $tasks = task::whereIn('class_id', $classes)->with(['teacher.user',])
            ->with(['task_answer' => function ($q) {
                $q->where('student_id', Auth::id());
            }])
            ->get();
        return view( 'panel_student\task', compact('tasks'));
    }

    public function answer(Request $request)
    {
        $this->validate($request, [
            'answer' => 'required_without:data-file',
            'answer_file' => 'required_without:data-answer',
        ]);
        $task_answer = task_answer::find($request->get('task_answer_id'));
        if (!$task_answer){
            $task_answer = new task_answer();
            $task_answer->student_id = Auth::id();
            $task_answer->task_id = $request->get('id');
        }
        $task_answer->answer = $request->get('answer');
        if ($request->hasFile('file')) {
            $fileName = time() . '.' . $request->answer_file->extension();
            $request->answer_file->move(public_path('upload/answers'), $fileName);
            $task_answer->file_name = $fileName;
        }
        $task_answer->save();
        return redirect(route('student.task.index'));
    }

}
