<?php

namespace App\Http\Controllers\ParentsPanelController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportCardController extends Controller
{
    public function index(Request $request)
    {
        return view('panel_parents\report_card');
    }
}
