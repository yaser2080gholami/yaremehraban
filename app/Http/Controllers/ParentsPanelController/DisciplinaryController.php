<?php

namespace App\Http\Controllers\ParentsPanelController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DisciplinaryController extends Controller
{
    public function index()
    {
        $this->authorize('Disciplinary.read');
        return view('panel_parents\disciplinary');
    }

}
