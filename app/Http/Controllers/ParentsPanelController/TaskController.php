<?php

namespace App\Http\Controllers\ParentsPanelController;

use App\Admin\school;
use App\Admin\SchoolClass;
use App\Admin\SchoolClassStudent;
use App\Admin\student;
use App\Admin\task;
use App\Admin\task_answer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function index()
    {
        $students = student::where('parent_id', Auth::id())->pluck('student_id');
        $classes = SchoolClassStudent::whereIn('student_id', $students)->pluck('school_class_id');
        $tasks = task::whereIn('class_id', $classes)->with(['school_class.course', 'teacher.user'])->get();
        return view('panel_parents.task', compact(['tasks']));
    }

    public function answers(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);
        $students = student::where('parent_id', Auth::id())->pluck('student_id');
        $task = task::find($request->get('id'));
        $answers = task_answer::whereIn('student_id', $students)->where('task_id', $request->get('id'))->with(['student.user'])->get();
        return view('panel_parents.task_answers', compact(['answers', 'task']));
    }
    public function downloadFile(Request $request)
    {
//        $this->authorize('task.add');
        $this->validate($request, [
            'id' => 'required'
        ]);
        $answer = task_answer::find($request->get('id'));
        $file = public_path() . "/upload/answers/".$answer->file_name;

        $headers = [
            'Content-Type: application/pdf',
            "Content-type: image/jpeg"
        ];

        return response()->download($file, $answer->file_name, $headers);;
    }


}
