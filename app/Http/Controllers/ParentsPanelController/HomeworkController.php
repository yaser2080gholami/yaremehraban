<?php

namespace App\Http\Controllers\ParentsPanelController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeworkController extends Controller
{
    public function index()
    {
        return view('panel_parents.homework');
    }

}
