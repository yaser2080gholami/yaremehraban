<?php

namespace App\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class permission extends Model
{
    protected $fillable = ['name','label'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
