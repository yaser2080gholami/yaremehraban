<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class calender extends Model
{
    protected $fillable = ['title','date','school_id'];
}
