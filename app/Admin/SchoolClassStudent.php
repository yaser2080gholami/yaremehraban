<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class SchoolClassStudent extends Model
{
    protected $fillable = [
        'school_class_id', 'student_id'
    ];

    protected $table = 'school_class_student';

}
