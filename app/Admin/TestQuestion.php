<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class TestQuestion extends Model
{
    protected $fillable = [
        'id', 'title', 'option1', 'option2', 'option3', 'option4', 'correct_option', 'homework_id', 'created_at', 'updated_at'
    ];

    public function answer_test_homework(){
        return $this->hasMany(answer_test_homework::class, 'question_id');
    }

    public function homework(){
        return $this->belongsTo(homework::class, 'id');
    }


}
