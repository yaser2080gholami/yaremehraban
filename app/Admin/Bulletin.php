<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model
{
    protected $fillable =[
      'school_id','id','key','value'
    ];
}
