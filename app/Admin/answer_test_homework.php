<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class answer_test_homework extends Model
{
    protected $fillable = [
        'id', 'question_id', 'student_id', 'answer', 'is_correct', 'created_at', 'updated_at'
    ];

    public function question()
    {
        return $this->belongsTo(TestQuestion::class, 'id');
    }

    public function student()
    {
        return $this->belongsTo(student::class, 'student_id');
    }
}
