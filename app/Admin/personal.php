<?php

namespace App\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class personal extends Model
{
    protected $fillable = ['personal_id','school_id','job_title','father_name','sheba'];

    public function user()
    {
        return $this->belongsTo(User::class,'personal_id');
    }
}
