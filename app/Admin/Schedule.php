<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'id', 'day', 'start_time', 'label', 'duration', 'class_id'
    ];

    public function school_class()
    {
        return $this->belongsTo(SchoolClass::class, 'id');
    }

}
