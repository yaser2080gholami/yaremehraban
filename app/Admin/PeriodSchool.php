<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class PeriodSchool extends Model
{
    protected $fillable =['school_id','period_id'];
    protected $table = 'period_school';
}
