<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class homework extends Model
{
    protected $fillable = [
        'id', 'lesson_name', 'type', 'question_number','title', 'course_id', 'teacher_id', 'start_date', 'expire', 'created_at', 'updated_at'
    ];

    protected $table = 'homeworks';

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function descriptive_question(){
        return $this->hasMany(DescriptiveQuestion::class, 'homework_id');
    }

    public function test_question(){
        return $this->hasMany(TestQuestion::class, 'homework_id');
    }
}
