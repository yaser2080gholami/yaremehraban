<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class task_answer extends Model
{
    protected $fillable = [
        'id', 'student_id', 'task_id','answer', 'file_name', 'grade', 'comment_teacher'
    ];

    public function student(){
        return $this->belongsTo(student::class, 'student_id', 'student_id');
    }

    public function task(){
        return $this->belongsTo(task::class, 'task_id', 'id');
    }
}
