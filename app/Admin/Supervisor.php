<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{
    protected $fillable = [
        'student_id', 'name' ,'family','national_id','phone','ratio','created_at', 'updated_at'
    ];

    public function student()
    {
        return $this->belongsTo(Supervisor::class);
    }
}
