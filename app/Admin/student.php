<?php

namespace App\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $fillable = [
       'course_id', 'name' ,'student_id','family','state','city','code','sex','level','national_id','school','phone','student_code', 'parent_id', 'school_id', 'class_id', 'created_at', 'updated_at'
    ];

    public function answer_homework_descriptive(){
        return $this->hasMany(answer_homework_descriptive::class, 'student_id');
    }

    public function task_answer(){
        return $this->hasMany(task_answer::class, 'student_id');
    }

    public function answer_test_homework(){
        return $this->hasMany(answer_test_homework::class, 'student_id');

    }

    public function user(){
        return $this->belongsTo(User::class, 'student_id', 'id');
    }

    public function parent(){
        return $this->belongsTo(User::class, 'parent_id', 'id');
    }

    public function school(){
        return $this->belongsTo(school::class, 'id');
    }

    public function classes(){
        return $this->belongsToMany(SchoolClass::class, 'school_class_student', 'student_id', 'school_class_id', 'student_id', 'id');
    }

    public function course(){
        return $this->belongsTo(Course::class,'course_id', 'id');
    }

    public function supervisor()
    {
        return $this->belongsTo(Supervisor::class);
    }
}
