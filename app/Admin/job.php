<?php

namespace App\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    protected $fillable =[
        'user_id','comment','deadline'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
