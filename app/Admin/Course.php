<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'id', 'name', 'created_at', 'updated_at'
    ];

    public function school_class(){
        return $this->hasMany(SchoolClass::class, 'course_id','id');
    }

    public function student()
    {
        return $this->hasMany(student::class,'course_id','id');
    }
}
