<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class OnlineClass extends Model
{
    protected $fillable = [
        'id', 'class_id', 'url'
    ];

    protected $table = 'online_classes';


    public function class()
    {
        return $this->belongsTo(SchoolClass::class);
    }
}
