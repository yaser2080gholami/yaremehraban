<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class ConfirmationCode extends Model
{
    protected $fillable = [
        'id', 'code', 'user_id', 'is_used', 'created_at', 'updated_at'
    ];

}
