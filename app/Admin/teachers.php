<?php

namespace App\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class teachers extends Model
{
    protected $fillable = [
        'teacher_id', 'school_id', 'created_at', 'updated_at', 'sheba_number', 'father_name'
    ];

    public function school_class(){
        return $this->hasMany(SchoolClass::class, 'teacher_id', 'teacher_id');
    }

    public function task(){
        return $this->hasMany(task::class, 'teacher_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }

    public function school(){
        return $this->belongsTo(school::class, 'id');
    }
}
