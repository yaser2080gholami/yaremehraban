<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class DescriptiveQuestion extends Model
{
    protected $fillable = [
        'id', 'title', 'grade', 'homework_id', 'created_at', 'updated_at'
    ];

    public function homework(){
        return $this->belongsTo(homework::class, 'id');
    }

    public function answer_homework_descriptive(){
        return $this->hasMany(answer_homework_descriptive::class, 'question_id');
    }
}
