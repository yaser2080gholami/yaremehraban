<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    protected $fillable = [
        'id', 'label', 'school_id', 'teacher_id', 'created_at', 'updated_at'
    ];

    protected $table = 'classes';

    public function homework(){
        return $this->hasMany(homework::class, 'class_id');
    }

    public function task(){
        return $this->hasMany(task::class, 'class_id');
    }

    public function student(){
        return $this->belongsToMany(student::class, 'school_class_student', 'school_class_id', 'student_id', 'id', 'student_id');
    }

    public function school(){
        return $this->belongsTo(school::class, 'id');
    }

    public function teacher(){
        return $this->belongsTo(teachers::class, 'teacher_id','teacher_id');
    }

    public function course(){
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function online(){
        return $this->hasMany(OnlineClass::class);
    }
}
