<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class YareMehrabanBulletin extends Model
{
    protected $fillable =[
        'id','content', 'is_active'
    ];
}
