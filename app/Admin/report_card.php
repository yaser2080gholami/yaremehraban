<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class report_card extends Model
{
    protected $fillable = [
        'id', 'name', 'school_id', 'created_at', 'updated_at'
    ];

    public function school(){
        return $this->belongsTo(school::class, 'id');
    }
}
