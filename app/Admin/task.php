<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class task extends Model
{
    protected $fillable = [
        'id', 'title', 'content', 'teacher_id', 'class_id', 'created_at', 'updated_at', 'deadline', 'lesson_name'
    ];

    public function teacher()
    {
        return $this->belongsTo(teachers::class, 'teacher_id', 'teacher_id');
    }

    public function school_class()
    {
        return $this->belongsTo(SchoolClass::class, 'class_id', 'id');
    }

    public function task_answer()
    {
        return $this->hasMany(task_answer::class, 'task_id', 'id');
    }
}
