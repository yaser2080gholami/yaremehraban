<?php

namespace App\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
    protected $fillable = [
        'id', 'user_id', 'priority', 'subject', 'destination_id', 'comment', 'file_name', 'ticket_id', 'created_at', 'updated_at'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }


    public function destination(){
        return $this->belongsTo(User::class, 'destination_id');
    }

    public function ticket(){
        return $this->belongsTo(ticket::class, 'ticket_id', 'id')->with('ticket');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'ticket_id');
    }

    public function grandchildren()
    {
        return $this->children()->with('grandchildren');
    }
}
