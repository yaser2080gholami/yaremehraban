<?php

namespace App\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class school extends Model
{
    protected $fillable = [
        'id', 'name', 'identity', 'description', 'manager_id', 'province', 'city', 'address', 'phone_number', 'created_at', 'updated_at'
    ];

    public function report_card(){
        return $this->hasMany(report_card::class, 'school_id');
    }

    public function school_class(){
        return $this->hasMany(SchoolClass::class, 'school_id');
    }

    public function student(){
        return $this->hasMany(student::class, 'school_id');
    }

    public function teacher(){
        return $this->hasMany(teachers::class, 'school_id');
    }

    public function manager(){
        return $this->belongsTo(User::class, 'id');
    }

    public function period()
    {
        return $this->belongsToMany(period::class);
    }
}
