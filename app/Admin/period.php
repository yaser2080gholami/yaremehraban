<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class period extends Model
{
    protected $fillable = ['name'];

    public function school()
    {
        return $this->belongsToMany(school::class);
    }
}
