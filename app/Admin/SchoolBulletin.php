<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class SchoolBulletin extends Model
{
    protected $fillable = [
        'school_id', 'id', 'content', 'is_active'
    ];
}
