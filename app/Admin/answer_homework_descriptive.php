<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class answer_homework_descriptive extends Model
{
    protected $fillable = [
        'id', 'question_id', 'student_id', 'answer', 'file_name', 'score'
    ];

    public function question()
    {
        return $this->belongsTo(DescriptiveQuestion::class, 'id');
    }

    public function student()
    {
        return $this->belongsTo(student::class, 'student_id');
    }
}
