<?php

namespace App;

use App\Admin\job;
use App\Admin\permission;
use App\Admin\personal;
use App\Admin\school;
use App\Admin\student;
use App\Admin\teachers;
use App\Admin\ticket;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'family', 'type', 'state', 'city', 'is_active', 'code', 'jender', 'level', 'national_id', 'school', 'phone', 'password', 'email_verified_at', 'type', 'remember_token', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function school()
    {
        return $this->hasMany(school::class, 'manager_id');
    }

    public function student()
    {
        return $this->belongsTo(student::class, 'student_id');
    }

    public function teacher()
    {
        return $this->belongsTo(teachers::class, 'teacher_id');
    }

    public function ticket()
    {
        return $this->hasMany(ticket::class, 'user_id');
    }
    public function permission()
    {
        return $this->belongsToMany(permission::class);
    }
    public function hasPermission($permission)
    {
        return $this->permission->contains('name',$permission->name );
    }
    public function jobs()
    {
        return $this->hasMany(job::class);
    }


    public function personal()
    {
        return $this->belongsTo(personal::class,'id');
    }

}
