@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">تابلو اعلانات</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info" style="min-height: 250px;">
                        <div class="inner">
                            <h4 style="padding-bottom:38px">حدیث هفته</h4>
                            <hr>
                            <p>{{$bulletin['hadis']?? ''}}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-heart-o"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success" style="min-height: 250px;">
                        <div class="inner">
                            <h4 style="padding-bottom:38px">سخن حکمت آمیز</h4>
                            <hr>
                            <p>{{$bulletin['sokhan']?? ''}}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-balance-scale"></i>
                        </div>

                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning" style="min-height: 250px;">
                        <div class="inner">
                            <h4 style="padding-bottom:38px">شعار هفته</h4>
                            <hr>
                            <p>{{$bulletin['shoar']?? ''}}</p>

                        </div>
                        <div class="icon">
                            <i class="fa fa-volume-off"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger" style="min-height: 250px;">
                        <div class="inner">
                            <h4 style="padding-bottom:38px">توصیه ها</h4>
                            <hr>
                            <p>{{$bulletin['tosiye'] ?? ''}}</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div>

    </section>
    <section class="col-lg-512 connectedSortable ui-sortable">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <div class="card bg-primary-gradient collapsed-card">
                        <div class="card-header no-border ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                <i class="fa fa-commenting-o mr-1"></i>
                                تابلو اعلانات مدرسه الکترونیکی یار مهربان
                            </h3>
                            <!-- card tools -->
                            <div class="card-tools">
                                <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"
                                        data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <div class="card-body ">
                            <ul class="mr-4">
                                @foreach($yare_mehraban_bulletins as $yare_mehraban_bulletin)
                                    <li>{{$yare_mehraban_bulletin->content}}</li>
                                    <form method="post"
                                          action="{{route("bulletin.delete-mehraban", $yare_mehraban_bulletin->id)}}">
                                        @csrf
                                        <button class="btn btn-danger d-flex fa-recycle">
                                            حذف
                                        </button>
                                    </form>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-sm">
                    <div class="card bg-info-gradient collapsed-card">
                        <div class="card-header no-border ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                <i class="fa fa-comment-o mr-1"></i>
                                تابلو اعلانات مدرسه {{$school->name}}
                            </h3>

                            <div class="card-tools">
                                
                                    <a href="{{route('bulletin.edit')}}" type="button" class="btn bg-info btn-sm">
                                        <i class="fa fa-plus"></i>
                                    </a>
                            
                                <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul class="mr-4">
                                @foreach($school_bulletins as $school_bulletin)
                                    <li>{{$school_bulletin->content}}</li>
                                    <form method="post"
                                          action="{{route("bulletin.delete-school", $school_bulletin->id)}}">
                                        @csrf
                                        <button class="btn btn-danger d-flex fa-recycle">
                                            حذف
                                        </button>
                                    </form>
                                @endforeach
                            </ul>
                        </div>

                        <!-- /.card-footer -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Map card -->

        <!-- /.card -->

        <!-- solid sales graph -->

        <!-- /.card -->
    </section>
@endsection
