@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">ارسال تیکت</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <div class="card-body">
                    <form>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="inputFirstname">نام</label>
                                <input type="text" class="form-control form-control-sm" id="inputFirstname" placeholder="نام" disabled="" value="علی علیان">
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>اولیت</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option>کم</option>
                                        <option selected="selected">متوسط</option>
                                        <option>زیاد</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                            <div class="form-group">
                                    <label>مقصد</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option>معلم</option>
                                        <option selected="selected">مشاور</option>
                                        <option>هم کلاسی</option>
                                        <option>مدیر</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>مقصد</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option>معلم</option>
                                        <option selected="selected">مشاور</option>
                                        <option>هم کلاسی</option>
                                        <option>مدیر</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>متن</label>
                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..."></textarea>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">انتخاب فایل</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">بارگذاری</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">ارسال</button>

                            <a href="https://google.com" class="btn btn-default">لغو</a>
                        </div>
                    </form>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection
