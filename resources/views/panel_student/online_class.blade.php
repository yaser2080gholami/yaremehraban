@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">کلاس آنلاین</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام دبیر برگزار کننده</th>
                                    <th>تاریخ و زمان شروع</th>
                                    <th>لینک</th>
                                </tr>
                                @foreach($classes as $class)
                                    <tr>
                                        <th>{{$class->id}}</th>
                                        <th>{{$class->class->teacher->user->name}} {{$class->class->teacher->user->family}}</th>
                                        <th>{{$class->created_at}}</th>
                                        <th>
                                                <a target="_blank" href="{{$class->url}}">لینک کلاس</a>
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
    </div>
    </div>
@endsection
