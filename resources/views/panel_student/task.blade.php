@extends('panel.master')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">تکلیف دانش آموز</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست تکالیف</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام درس</th>
                                            <th>عنوان تکلیف</th>
                                            <th>معلم</th>
                                            <th>تاریخ تخصیص</th>
                                            <th>نظر معلم</th>
                                            <th>نمره</th>
                                            <th>پاسخ</th>
                                        </tr>
                                        @foreach($tasks as $task)
                                            <tr>
                                                <td>{{$task->id}}</td>
                                                <td>{{$task->lesson_name}}</td>
                                                <td>{{$task->title}}</td>
                                                <td>{{$task->teacher->user->name}} {{$task->teacher->user->family}}</td>
                                                <td>{{$task->deadline}}</td>
                                                <td>
                                                    @if(isset($task->task_answer[0]->comment_teacher))
                                                        {{
                                                            $task->task_answer[0]->comment_teacher
                                                        }}
                                                    @else {{''}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($task->task_answer[0]->grade))
                                                        {{
                                                            $task->task_answer[0]->grade
                                                        }}
                                                    @else {{''}}
                                                    @endif
                                                <td>
                                                    <button id="answer-modal" type="button" class="btn btn-primary"
                                                            data-toggle="modal"
                                                            title="مشاهده و پاسخ" data-target="#modal-default"
                                                            data-id="{{$task->id}}"
                                                            data-content="{{$task->content}}"
                                                            data-answer="{{$task->task_answer[0]->answer??''}}"
                                                            data-task-answer-id="{{$task->task_answer[0]->id ?? ''}}"
                                                    ><i
                                                            class="fa fa-eye"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-default" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">پاسخ تکلیف</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="post" action="{{route('student.task.answer')}}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label>تکلیف</label>
                                    <input id="data-content" type="text" class="form-control"
                                           placeholder="وارد کردن اطلاعات ..."
                                           value="در مورد کوه الوند توضیح دهید" disabled="">
                                    <input id="data-id" type="hidden" name="id">
                                    <input id="data-task-answer-id" type="hidden" name="task_answer_id">
                                </div>
                                <div class="form-group">
                                    <label>پاسخ</label>
                                    <textarea id="data-answer" class="form-control" rows="3"
                                              placeholder="پاسخ را وارد کنید ..." name="answer"></textarea>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group">
                                        <label for="answer_file">ارسال فایل</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                {{--                                                <label class="custom-file-label" for="exampleInputFile">انتخاب--}}
                                                {{--                                                    فایل</label>--}}
                                                <input type="file" id="data-file" name="answer_file">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.content -->
    <script>
        $(document).ready(function () {
            $('body').on('click', '#answer-modal', function () {
                document.getElementById('data-content').value = $(this).attr('data-content');
                document.getElementById('data-answer').innerHTML = $(this).attr('data-answer');
                document.getElementById('data-id').value = $(this).attr('data-id');
                document.getElementById('data-task-answer-id').value = $(this).attr('data-task-answer-id');
            });
        });
    </script>
@endsection
