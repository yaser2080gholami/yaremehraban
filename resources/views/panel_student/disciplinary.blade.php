@extends('panel.master')
@section('content')
    <button onclick="topFunction()" id="myBtn" title="Go to top" style="display: none;"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
    <script>
        //Get the button:
        mybutton = document.getElementById("myBtn");
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};
        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>
    <!-- Select2 style sheet -->
    <link rel="stylesheet" href="http://yaremehraban.org/web_src/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="http://yaremehraban.org/web_src/plugins/persian-calendar/css/persianDatepicker-default.css">
    <!-- Content Wrapper. Contains page content -->

        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">مورد انضباطی دانش آموز</h1>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">

                <div class="card">
                    <!-- <div class="card-header">
                          </div> -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">

                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">لیست موارد</h3>

                                        <div class="card-tools">
                                            <div class="input-group input-group-sm" style="width: 150px;">
                                                <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive p-0">
                                        <table class="table table-hover table-striped">
                                            <tbody>
                                            <tr>
                                                <th>ردیف</th>

                                                <th>تاریخ</th>
                                                <th>عنوان انضباطی</th>
                                                <th>مقدار کسر نمره</th>
                                                <th>درج کننده</th>

                                                <th>توضیحات</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>

                                                <td>98/11/24</td>
                                                <td>تاخیر</td>
                                                <td>1</td>
                                                <td>علی هلیان - معلم</td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">توضیحات</button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class="modal fade" id="modal-default" style="display: none; z-index: 3001;" aria-hidden="true">
                    <div class="modal-dialog" style="max-width: 750px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">مورد انضباطی </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>نمونه توضیحات مورد انضباطی</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>

                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            </div>

        </div>
        <!-- /.content -->
    <!-- /.content-wrapper -->

    <!-- Select2 -->
    <script src="http://yaremehraban.org/web_src/plugins/select2/select2.full.min.js"></script>
    <script src="http://yaremehraban.org/web_src/plugins/persian-calendar/js/persianDatepicker.min.js"></script>
    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
        $(function() {
            $("#input1, #input2").persianDatepicker();
        });
    </script><!-- Main Footer -->
@endsection