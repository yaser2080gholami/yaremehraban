@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">آزمون تستی دانش آموز</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست آزمون</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام درس</th>
                                            <th>عنوان آزمون</th>
                                            <th>تعداد سوال</th>
                                            <th>تاریخ شروع</th>
                                            <th>مدت زمان</th>
                                            <th>مشاهده و پاسخ</th>
                                        </tr>
                                        @foreach($homeworks as $homework)
                                            <tr>
                                                <th>{{$homework->id}}</th>
                                                <th>{{$homework->lesson_name}}</th>
                                                <th>{{$homework->title}}</th>
                                                <th>{{$homework->question_number}}</th>
                                                <th>{{$homework->start_date}}</th>
                                                <th>{{$homework->expire}}</th>
                                                <td>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                            title="مشاهده و پاسخ" data-target="#modal-default"><i
                                                            class="fa fa-eye"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-default" style="display: none; z-index: 3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">پاسخنامه تستی</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card direct-chat direct-chat-primary">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled=""></textarea>
                                        </div>

                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer ">
                                        <div class="form-group row" style="margin-bottom: 0rem;">
                                            <label style="padding-top:5px;">پاسخ: </label>
                                            <select style="padding-right:15px;margin-right:15px;"
                                                    class="form-control form-control col-5 form-control-sm">
                                                <option>گزینه ۱</option>
                                                <option>گزینه ۲</option>
                                                <option>گزینه ۳</option>
                                                <option>گزینه ۴</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.card-footer-->
                                </div>
                                <div class="card direct-chat direct-chat-primary ">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 2</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled=""></textarea>
                                        </div>

                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer ">
                                        <div class="form-group row" style="margin-bottom: 0rem;">
                                            <label style="padding-top:5px;">پاسخ: </label>
                                            <select style="padding-right:15px;margin-right:15px;"
                                                    class="form-control form-control col-5 form-control-sm">
                                                <option>گزینه ۱</option>
                                                <option>گزینه ۲</option>
                                                <option>گزینه ۳</option>
                                                <option>گزینه ۴</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->

                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت نهایی</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@endsection
