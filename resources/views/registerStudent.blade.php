@include('panel.frontend.css')
@include('panel.frontend.js')
<link rel="stylesheet" href="/css/hadi-css.css">
<body class="login-page">
<div class="login-box">
    <div class="card haditopcardui">
        <div class="row pt-1">
            <div class="col-12 text-center">
                <img width="60px" src="/img/logo-yaremehraban.png">
            </div>
        </div>
        <div class="login-logo mb-0">
            <a href="../../index2.html"><b><span class="hadiwhitecolor hadititle" style="font-size:0.7em;">ثبت نام دانش آموز</span></b></a>
        </div>
    </div>
    <!-- /.login-logo -->
    <div class="card hadicardmiddle rounded-0">
        <div class="card-body login-card-body">
            <p>لطفا اطلاعات زیر را تکمیل و روی دکمه تایید، کلیک کنید.</p>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('infoStudent')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="name" placeholder="نام">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="family" placeholder="نام خانوادگی">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <select class="form-control" name="state" id="">
                                <option value="1">انتخاب استان</option>
                                <option value="2">تهران</option>
                                <option value="3">قم</option>
                                <option value="4">اراک</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <select class="form-control" name="city" id="">
                                <option value="1">انتخاب شهر</option>
                                <option value="2">خرم آباد</option>
                                <option value="3">بروجرد</option>
                                <option value="4">مشهد</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <select class="form-control" name="sex" id="">
                                <option value="1">جنسیت</option>
                                <option value="2">مرد</option>
                                <option value="3">زن</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">

                            <select class="form-control" name="course_id" placeholder="شهر">
                                <option value="" disabled selected>انتخاب پایه تحصیلی</option>
                                @foreach($courses as $course)
                                    <option value="{{$course->id}}">{{$course->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="number" class="form-control" name="phone" placeholder="شماره موبایل">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="number" class="form-control" name="national_id" placeholder="کد ملی">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <select class="form-control" name="school" id="">
                                <option value="" disabled selected>مدرسه</option>
                                @foreach($schools as $school)
                                    <option value="{{$school->id}}">{{$school->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="new-password" placeholder="رمز عبور">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" required autocomplete="new-password" placeholder="تکرار رمز عبور">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-6">
                        <p class="pt-2 text-center">
                            <a href="">بازگشت</a>
                        </p>
                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" style="border-radius: 5px;">
                            تایید
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    @media only screen and (min-width: 768px) {
        .login-box {
            width: 700px;
        }
    }
</style>
</body>