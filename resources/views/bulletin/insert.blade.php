@extends('panel.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"></h1>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">

                <div class="card">
                    <!-- <div class="card-header">
                          </div> -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <form action="{{route('bulletin.add')}}" method="post">
                                        @csrf
                                        <div class="card-body">
                                            <div class="col-10">
                                                <div class="form-group col-5">
                                                    <label>بخش</label>
                                                    <select name="key" class="form-control form-control-sm" style="width: 100%;">
                                                        <option value="hadis" selected="selected">حدیث هفته </option>
                                                        <option value="sokhan">سخن حکمت آمیز</option>
                                                        <option value="shoar">شعار هفته</option>
                                                        <option value="tosiye">توصیه ها</option>
                                                        <option value="school_bulletin">تابلو اعلانات مدرسه</option>
                                                        
                                                            <option value="yare_mehraban_bulletin">تابلو اعلانات یار مهربان</option>
                                                
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>محتوا</label>
                                                    <textarea class="form-control" rows="10" name="value" placeholder="وارد کردن اطلاعات ..."></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-block float-left btn-success col-3">درج</button>
                                            </div>

                                        </div>
                                    </form>


                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
        </div>
        <!-- /.content -->
    </div>
@endsection
