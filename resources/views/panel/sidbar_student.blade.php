<aside class="main-sidebar sidebar-dark-primary elevation-4" style="">
    <!-- Sidebar -->
    <div class="sidebar" style="">
        <div>
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image mx-auto d-block">
                    <img src="http://yaremehraban.org/web_src/dist/img/logo-yaremehraban.png" alt="User Image">
                </div>
            </div>
            <div class="user-panel pb-3 mb-3 d-flex text-center">
            <div class="w-100 info">
              <a href="#" class="d-block">شبکه آنلاین یار مهربان<br>(مدرسه امام علی)</a>
            </div>
          </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu-close">
                        <a href="" class="nav-link active">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>
                                منو ها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('dashboard')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>داشبورد</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('student.calender.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تقویم</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('student.online.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>کلاس آنلاین</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('student.task.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تکلیف</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i class="fa fa-circle-o nav-icon"></i> <span>آزمون</span>
                                </a>
                            </li>
                                <li class="nav-item">
                                    <li class="active"><a class="nav-link" href="{{route('student.homework.index_test')}}"><i class="fa fa-circle-o"></i>آزمون تستی دانش آموز</a></li>
                                    <li><a class="nav-link" href="{{route('student.homework.index_descriptive')}}"><i class="fa fa-circle-o"></i>آزمون تشریحی</a></li>
                                </li>
                            <li class="nav-item">
                                <a href="{{route('student.report_card.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>کارنامه</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('bulletin')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تابلو اعلانات</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('student.disciplinary.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مورد انضباطی</p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                            گزارش و نمودار <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>ارزیابی عملکرد مطالعاتی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>نسبت مطالعه به نمره</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineClass" class="nav-link">
                                <i class=""></i>
                                <p>پیشرفت تحصیلی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>ثبت مطالعه درسی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>نمودار مقایسه کلاسی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>ارزیابی آموزشی</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                           پشتیبانی <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>ارسال تیکت</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>فهرست تیکت ها</p>
                            </a>
                        </li>
                    </ul>
                </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
