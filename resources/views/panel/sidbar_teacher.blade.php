<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
        <div>
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="row">
                    <div class="col-12 text-center"><img src=""/></div>
                    <div class="col-12 text-center" style="font-size: 14px; color: #c2c7d0;">شبکه آنلاین یار مهربان
                    </div>
                    <div class="col-12 pb-3 text-center" style="font-size: 14px; color: #c2c7d0;">(مدرسه کمالی)</div>
                    <div class="col-12 text-center image">
                        <img src="{{asset('img/logo-yaremehraban.png')}}" class="img-circle elevation-2"
                             alt="User Image">
                    </div>
                    <div class="col-12 text-center info">
                        <a href="#" class="d-block"> <span style="font-size: 12px"></span></a>
                    </div>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu-close">
                        <a href="" class="nav-link active">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>
                                منو ها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('dashboard')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>داشبورد</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('teacher.calender.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تقویم</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('teacher.online.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>کلاس آنلاین</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('teacher.task.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تکلیف</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('teacher.homework.index')}}">
                                    <i class="fa fa-circle-o nav-icon"></i> <span>آزمون</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('teacher.report_card.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>کارنامه</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('bulletin')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تابلو اعلانات</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('teacher.disciplinary.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مورد انضباطی</p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                            گزارش و نمودار <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>ارزیابی عملکرد مطالعاتی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>نسبت مطالعه به نمره</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineClass" class="nav-link">
                                <i class=""></i>
                                <p>پیشرفت تحصیلی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>ثبت مطالعه درسی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>نمودار مقایسه کلاسی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>ارزیابی آموزشی</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                           پشتیبانی <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>ارسال تیکت</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>فهرست تیکت ها</p>
                            </a>
                        </li>
                    </ul>
                </li>
                        </ul>
                    </li>
                            </ul>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
