<aside class="main-sidebar sidebar-dark-primary" style="min-height: 2650px !important; position:absolute;">
    <!-- Sidebar -->
    <div class="sidebar" style="height: 2536px;background-color: #003976!important;font-size: small!important;">
        <div>
            <div class="user-panel mt-3 mb-1 d-flex">
                <div class="image mx-auto d-block">
                    <img src="http://yaremehraban.org/web_src/dist/img/logo-yaremehraban.png" alt="User Image">
                </div>
            </div>
            <div class="user-panel pb-3 mb-3 d-flex text-center">
                <div class="w-100 info">
                    <a href="#" class="d-block">شبکه آنلاین یار مهربان<br>(مدرسه امام علی)</a>
                </div>
            </div>
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEX///8AAAC+vr6CgoJwcHDr6+vMzMz5+fnf39/19fXy8vL8/PzZ2dlzc3Pj4+Pm5uaNjY3Q0NC4uLhPT096enpDQ0MSEhJnZ2c4ODipqamioqIqKipZWVkKCgqUlJQ/Pz9hYWGlpaVLS0smJibDw8MxMTEeHh5/f3+QkJAbGxs6Ojqhyg2HAAAIRUlEQVR4nO2d25qiMAyA1wOCigiezzqOOuu8/wPujDZtUVQoCa375b/Ym5XSQJsTaebPH4ZhGIZhGIZhGIZhUOmErfrgl3or7NieDDJ+sugta2mWvUXLtz0xFPwwHtYeMYzDd5cyGd2+u1uWo8T2JM1pLz5fiCeEXLRtT9WIaJRLvCujyPZ0CxP1Csj3S++9ZPQbmVJsd81R/NXbbSdZ/9t4I6UzuJv9cDPop37Sry/uVezA0nyL4q3S8x5vHmnLZLNP/3TlVTpTQwZrfc7HTfD018EmbU7e4DWmNOi2nuOKw1i/ZEQ+w3J0tvrmC3NeFepbcuu0zxqcte3XKnDh4VtdeHq+rq2SaK/io+C1H9q1zvpxLTXHffGl5ml6tcjrrxBNwIXRAAs1wAF5biioJXo2XWUt5es4uFD7cnJHc7PtKePYf/3ramnLx78qM0xX2o2JaxGV9NSmJQfawUBDlHmhEcO8dqWHks8qLj0UIi3MBy+thkM2w4c5bbsYo81gOHcCxqmY0Ron+JFquYkyHAIH7GUlF70jht8/ivls0IYExXXEWPXlAWdrjzgmaBsz9w+ZDiwpzKAnhEFdsPsx+hrVR3XAKLbBy0Ied+LMS9yImWCnkAYkS8MA/yRsPfrIwu5/2jb78KjzJNWKUSdaHEURfvIM33B1RXKqVDRWHnCw5gRjz8XYdmNhsPYUOU4wtHatvlhJZcPebJrXwb9JBs8JPGaaSK5FuEDyApqUaHgHtKlYR1RxHPHweTjTPmSxRM5Ew+cgIt4osM3tfeIXqmBJFad2j5SKLA8b6n3Ss20Rp9QT2FCa2zyIFDxdvkjkuKylvyEFRffNNrje4GgrgvJEHE5XIiIc+4mtIhRx/086r6pzshteRORrqDuzaxDFLhkT3mJMvdOfE9Brur0TEmLmum8Z/vcSurFKCfchZKNsSUivS8GnsKVL6e0xvU/xHHp7LFbJyVqmRpRa5i2yLE5Ib3GfIxLeFOngKyKNYS/tLeqB6b7xiZJ/e3XDIu++I7uBKJCyl06EckQqcwF1OvYKFSEXRqVq4Gu+xaS3MMhFK57zIr77zIiGz0NMq83H1JrsNQfSZUT73ScnNUplB59ISQbPy5QygBLhr71s6S9QTkChTUGT4hdBFKEtjnE1CMYWHtPacs0QHOTC1zWgZ2wf9YKlhF+6BMVW1k9eQJ0ktucGr5AyC5QP0DXYOxFOEtvVMxdEoI+cS4FSpBPqqGbAS8QNU4fuvEJZNYQa6kMZi9VqIYmsrMfLiMnzCI4cKoHjFnhqD84+23XYFB48cSx92sBfFSWBIAApxpAtGeiSeIWRpwYxHBB5GNVy7WyKjuyjUP4bSgBDrZ06mK9OOZc1/JEcyRE9Cshz2OtyoWIoT9s6cSBIpymffZm9qNaCMyfzFKq3hbkKVI1tHDsFfMFXPRG+zEbofikBbZ8jyaSturDsTVRqpK4f2z/tlImvtW4prie0nhEox4lp0PonfRdTOKHWvsUlS3+H3mGomf/rd7+pXWe4i6tirk01bw+2INV5ySFnNJvkrE9399oxae30C4z7olSIr6+4Wu30N3is+f0gTjenazppJe4Y3LTUm43qWdYjOvSO6R+ubZ81zE3nvinkchgPDqH3awZ8LzwM4tV928+RU8HEC6LHXVkfMXyv1pA/CmT1WihdPsdipVyE09eCCaZvoEEz8TaZbTxvWG+cSTg9ofvIVU7i2VPxZo1Hr88t53vxfV49XGjBvPmdLV1z/jAGmY8nQ0d6t/zgX72SJ58Yul7yEU9l69nzdhovEu+Jdb+WsVjvpyAIoOMznr2GUHjlhIujtdTFcpqV3/dJV7uaFy3zUENK67dTlpSqoiwvnt6RFWdZBTdtzndWV+p90+fSef05/pDm+Jld1+MySRZvlzWkLZ0aaLFPT2tRfjR3MedaH+kvrav7ykrUoa/QebpL7tTMDQv17t6L9IKtXqd2tSd8vHgmB72Pd6P4Q49SjvrFnwm1GLJqnarr0JFQdl6qwXpcLNwLU7mPsUjTtTWpm5UmUfWmz5on81efZW2Vfz/W093nNbOqrdRthb64dttZKima3CRe4jzbp3Wjkpcp6xAq8zip7AhbrGZzVzi4Sc+2dh4N+o9NdjuaN28uqP29WY2+tlIrija0R57hiEb3kf1sF9eTKKV7ul6QDBoZ2ahdxnvSvmhUkoxTEjz43Jvs7+Z9+fVpuTwel5+Xf86ZP6kds90XrVN4BRlx5XTsH+78pHim7TLgw0Xoqf1N/vFbvcGntbtB0T8z82MNnmmlrnqwxG9RxUqvfMXOfJwlxwNmH6/cIHVn0nJFpSnzPElvsc2S5o5Xf8fkijK2hB5cXd4kr05r1xv7dZZUwHhUz+vESpVKd/BYtc4vtFC8cNBoDm8Nw+e+2ZiHheb6AdeS1X7LjWVilfx2p+P1+97PP5HX6bRNInf5FokCRrkJ7X2llXuRZCvKgjOK8zF5AWNFch4QBrdarSR7hxOsIzgcg9RZ3hSozTzhR4vgiNn+EA3aBj3mh11ov5pHfMvC7q4ti8vt/7UpCDSwPxqLcGeHPKwJwiwjF6BCYsb+hxJ5vHqCm+0XS+PbhaJBaJKOq9SFhLbPc15pEkrYQx3UFJbQCJawUlhCI1jCSmEJjWAJK4UlNIIlrBSW0AiWsFJYQiNYwkphCY0QErpxSJ4lNIIlrBSW0AiWsFJYQiNYwkphCY1gCSuFJTSCJayU/z8C7hFKOGu4wIxQQqdgCVnCNMnrG1YNcl1b9/T6lhWDbbmC17esljF6EWH3UHeJd21KxDAMwzAMwzAMwzAMwzAMY49/ZtRcgh5Fw+sAAAAASUVORK5CYII=" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">علی علوی</a>
                </div>
            </div>
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/mainDashboard" class="nav-link hadipadding">
                        <i class="fa fa-address-card"></i>
                        <p>
                            داشبورد </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            تقویم </p>
                    </a>
                </li>
                <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                            ایجاد مدرسه آنلاین <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>تعریف معلم</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>تعریف دانش آموز</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineClass" class="nav-link">
                                <i class=""></i>
                                <p>تعریف کلاس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>تعریف پرسنل</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            کلاس آنلاین </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            تکالیف </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            آزمون </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            کارنامه </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            پیش ثبت نام </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            تکمیل پرونده پرسنلی </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            تابلو اعلانات </p>
                    </a>
                </li>
                <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                            مالی <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>اعلام نرخ حق التدریس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>تعریف هزینه درس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineClass" class="nav-link">
                                <i class=""></i>
                                <p>تعریف حقوق پرسنل</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>گزارش شهریه دانش آموز</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>گزارش مالی معلم</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>لیست اموال</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                            مدرسه <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>تقویم اجرایی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineClass" class="nav-link">
                                <i class=""></i>
                                <p>برنامه کلاسی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>زمانبندی کل کلاس ها</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>معلم های یک کلاس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>برنامه هفتگی کلاس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>برنامه ریزی امتحانات</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>کلاس بنده دانش آموزان</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>ارزیابی آموزشی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>موارد انضباطی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>اطلاعات کلاس ها</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>پروفایل مجتمع و مدرسه</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>سفارش کالا و خدمات</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            اردو </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            اعلام نمره دهی </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            غیر فعال سازی کاربران </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>گزارشات مالی </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            درج در تابلو اعلانات </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                           مورد انضباطی </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            کارنامه </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                           غیر فعال سازی مدارس </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            سرویس هوشمند </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            ارزیابی آموزشی</p>
                    </a>
                </li>
                <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                            گزارش <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>پرونده دانش آموز</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>پیشرفت تحصیلی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineClass" class="nav-link">
                                <i class=""></i>
                                <p>ثبت مطالعه درسی</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>مقایسه معدل کلاس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>عملکرد امتحانات یک درس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>مجتمع و مدارس</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>مقایسه مطالعه دانش آموز</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineEmployee" class="nav-link">
                                <i class=""></i>
                                <p>نمودار مقایسه کلاسی</p>
                            </a>
                        </li>
                    </ul>
                    </li>
                    <li class="nav-item has-treeview menu-close menu-close">
                    <a href="#" class="nav-link hadipadding">
                        <i class="fa fa-plus-square-o"></i>
                        <p>
                            سرویس <i class="right fa fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/defineTeacher" class="nav-link">
                                <i class=""></i>
                                <p>تاخیر</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item" style="background-color: #0358a0;">
                            <a href="http://yaremehraban.org/panel/main/createStudent" class="nav-link">
                                <i class=""></i>
                                <p>گزارشات هفتگی ماهانه کل</p>
                            </a>
                        </li>
                    </ul>                    
                    </li>
                    <li class="nav-item">
                    <a href="http://yaremehraban.org/panel/main/calendar" class="nav-link hadipadding">
                        <i class=""></i>
                        <p>
                            پشتیبانی </p>
                    </a>
                </li>
                
                
            </ul>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>