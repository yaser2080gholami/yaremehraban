<nav class="main-header navbar navbar-expand navbar-light border-bottom bg-dark" style="background-color: #003976!important;">    
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a class="nav-link" data-widget="pushmenu" href="#">13:16 1399/6/1 </a>
        </li>
      </ul>
      <ul class="navbar-nav mx-auto d-md-none">
        <li class="nav-item"><span style="font-size:10pt;">(مدرسه )</span></li><li>
      </li>
      </ul>
      <!-- Right navbar links -->
      <ul class="navbar-nav mr-auto">
        <!-- Messages Dropdown Menu -->
        <!--
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fa fa-comments-o"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left bg-dark">
            <a href="#" class="dropdown-item">
              <div class="media">
                <img src="http://yaremehraban.org/web_src/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 ml-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    حسام موسوی
                    <span class="float-left text-sm text-danger"><i class="fa fa-star"></i></span>
                  </h3>
                  <p class="text-sm">با من تماس بگیر لطفا...</p>
                  <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 ساعت قبل</p>
                </div>
              </div>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <div class="media">
                <img src="http://yaremehraban.org/web_src/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle ml-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    پیمان احمدی
                    <span class="float-left text-sm text-muted"><i class="fa fa-star"></i></span>
                  </h3>
                  <p class="text-sm">من پیامتو دریافت کردم</p>
                  <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 ساعت قبل</p>
                </div>
              </div>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <div class="media">
                <img src="http://yaremehraban.org/web_src/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle ml-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    سارا وکیلی
                    <span class="float-left text-sm text-warning"><i class="fa fa-star"></i></span>
                  </h3>
                  <p class="text-sm">پروژه اتون عالی بود مرسی واقعا</p>
                  <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i>4 ساعت قبل</p>
                </div>
              </div>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">مشاهده همه پیام‌ها</a>
          </div>
        </li>
        -->
        <!-- Notifications Dropdown Menu -->
        <!--
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fa fa-bell-o"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left bg-dark">
            <span class="dropdown-item dropdown-header">15 نوتیفیکیشن</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fa fa-envelope ml-2"></i> 4 پیام جدید
              <span class="float-left text-muted text-sm">3 دقیقه</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fa fa-users ml-2"></i> 8 درخواست دوستی
              <span class="float-left text-muted text-sm">12 ساعت</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fa fa-file ml-2"></i> 3 گزارش جدید
              <span class="float-left text-muted text-sm">2 روز</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">مشاهده همه نوتیفیکیشن</a>
          </div>
        </li>
        -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user-o" style="font-size:25px;" aria-hidden="true"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left bg-dark">
            <a href="#" class="dropdown-item">
              علی علوی              (مدیر مدرسه)
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fa fa-user-circle ml-2" aria-hidden="true"></i>تغییر مشخصات کاربری
            </a>
            <a href="#" class="dropdown-item">
              <i class="fa fa-refresh ml-2"></i> تغییر رمز عبور
            </a>
            <a href="http://yaremehraban.org/panel/main/logout" class="dropdown-item">
              <i class="fa fa-sign-out ml-2"></i>خروج
            </a>
          </div>
        </li>
      </ul>      
</nav>


