@extends('panel.master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">پرداخت شهریه والدین</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <p>شهریه برای سال تحصیلی 99 مبلغ 2.000.000 تومان است</p>
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default">
                                    پرداخت کامل
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست شهریه</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام قسط</th>
                                            <th>مبلغ</th>
                                            <th>مدت زمان سررسید</th>
                                            <th>عملیات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>قسط اول</td>
                                            <td>1.500.000</td>
                                            <td>1.12.98</td>
                                            <td>
                                                <button type="button" class="btn btn-primary">پرداخت</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>قسط دوم</td>
                                            <td>1.000.000</td>
                                            <td>1.1.98</td>
                                            <td>
                                                <button type="button" class="btn btn-primary">پرداخت</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>

    </div>
    <!-- /.content -->
@endsection
