@extends('panel.master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">کارنامه دانش آموز</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست کارنامه های موجود</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام کارنامه</th>
                                            <th>معدل</th>
                                            <th>وضعیت</th>
                                            <th>جزئیات کارنامه</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>مستمری بهمن</td>
                                            <td>19.52</td>
                                            <td>قبولی</td>
                                            <td>
                                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-test-questions" title="جزئیات کارنامه"><i class="fa fa-check"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <!-- .modal-default -->
        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-default" style="display: none;z-index:3005;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">لیست نمرات کلاس 207</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">

                                <p>نام درس : ریاضی</p>
                                <p>دوره و پایه : اول متوسطه پایه دوم</p>
                                <p>وضعیت : اعلام شده</p>

                                <div class="table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>نمره</th>
                                            <th>توضیحات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی اسدی</td>
                                            <td>17</td>
                                            <td>آفرین! خوب بود
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">پرینت<i class="fa fa-print" style="padding-right:5px;"></i></button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-view" style="display: none;z-index:3005;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">درج نمره</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">

                                <div class="table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>نمره</th>
                                            <th>توضیحات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی اسدی</td>
                                            <td>
                                                <div class="col-9">
                                                    <input id="score" type="text" class="form-control" placeholder="نمره">
                                                </div>
                                            </td>
                                            <td>

                                                <input type="text" class="form-control" placeholder="توضیحات">


                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت نهایی</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-descriptive-questions" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">طرح سوال آزمون تشریحی</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="form-control" style="border:1px solid #ced4da;padding:10px;margin-bottom: 10px;">
                                <div class="mx-auto" style="width: 100%;margin:0px;">
                                    <div class="form-group">
                                        <label>سوال</label>
                                        <textarea class="form-control" rows="3" placeholder="لطفا سوال را وارد کنید"></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">

                                            <input type="text" class="form-control col-3" placeholder="بارم">

                                        </div>
                                        <div class="col-sm-6">

                                            <button type="button" class="btn btn-danger">اضافه کردن</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body table-responsive p-0">
                                <div class="form-control" style="border:1px solid #ced4da;padding:10px;">
                                    <label>لیست سوال ها</label>
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>سوال</th>
                                            <th>بارم</th>
                                            <th>عملیات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی علیان</td>
                                            <td>1234567899</td>
                                            <td>
                                                <button type="button" class="btn btn-success"><i class="fa fa-edit" title="ویرایش"></i></button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash" title="حذف"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>

                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-test-questions" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 1000px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">کارنامه</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="modal-body" style="padding:4px;">
                        <p>نام و نام خانودگی دانش آموز : محمد رضایی</p>
                        <p>کارنامه : مستمری بهمن</p>
                        <p>دوره و پایه : متوسطه اول</p>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام درس</th>
                                    <th>واحد</th>
                                    <th>نمره</th>
                                    <th>نمره با ضریب</th>
                                    <th>وضعیت قبولی</th>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>ریاضی</td>
                                    <td>2</td>
                                    <td>18</td>
                                    <td>36</td>
                                    <td>پاس شده</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->


                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">

                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">پرینت<i class="fa fa-print" style="padding-right:5px;"></i></button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        </div>

        <div class="modal fade" id="modal-descriptive-check" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">لیست برگه های پاسخنامه آزمون تشریحی کلاس 304</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">

                            <div class="card-body table-responsive p-0">
                                <div class="form-control" style="border:1px solid #ced4da;padding:10px;">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>نمره</th>
                                            <th>عملیات تصحیح</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی علیان</td>
                                            <td>19 از 20</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-descriptive-check-feedback" title="ویرایش"><i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-descriptive-check-feedback" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">برگه پاسخنامه دانش آموز: محمد اناری</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning" data-original-title="3 New Messages">3 نمره</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="بلندترین قله جهان اورست است" value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..." disabled></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نمره</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="نمره">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نظر معلم</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="نمره">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning" data-original-title="3 New Messages">3 نمره</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 2</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="بلندترین قله جهان اورست است" value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..." disabled></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نمره</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="نمره">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نظر معلم</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="نمره">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت نهایی</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.content -->
@endsection
