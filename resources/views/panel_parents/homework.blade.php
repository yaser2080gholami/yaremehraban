@extends('panel.master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">مشاهده آزمون والدین</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست آزمون</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام درس</th>
                                            <th>عنوان آزمون</th>
                                            <th>نوع آزمون</th>
                                            <th>مشاهده</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>ریاضی</td>
                                            <td>میان ترم ریاضی</td>
                                            <td>تشریحی</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" title="و پاسخ" data-target="#modal-descriptive-exam"><i class="fa fa-eye"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>علوم</td>
                                            <td>میان ترم علوم</td>
                                            <td>تستی</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" title="و پاسخ" data-target="#modal-test-exam"><i class="fa fa-eye"></i></button>
                                            </td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-descriptive-exam" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">پاسخنامه تشریحی</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning" data-original-title="3 New Messages">x از y</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="بلندترین قله جهان اورست است" value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..." disabled></textarea>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning" data-original-title="3 New Messages">1 از 2</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 2</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="بلندترین قله جهان اورست است" value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..." disabled></textarea>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <div class="info-box">

                            <div class="info-box-content">
                                <span class="info-box-text">نمره دانش آموز:</span>
                                <span class="info-box-number">17 از 20</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-test-exam" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">پاسخنامه تستی</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card direct-chat direct-chat-primary  collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-success" data-original-title="پاسخ صحیح است"> <i class="fa fa-check"></i></span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="بلندترین قله جهان اورست است" value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>

                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer ">
                                        <div class="form-group row" style="margin-bottom: 0rem;">
                                            <label style="padding-top:5px;">پاسخ دانش آموز:</label>
                                            <p style="padding-top:5px;padding-right:10px">گزینه 2</p>
                                            <label style="padding-top:5px;padding-right:30px">پاسخ صحیح:</label>
                                            <p style="padding-top:5px;padding-right:10px">گزینه 2</p>
                                        </div>
                                    </div>
                                    <!-- /.card-footer-->
                                </div>
                                <div class="card direct-chat direct-chat-primary collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-danger" data-original-title="پاسخ صحیح اورست است"> <i class="fa fa-close"></i></span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 2</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="بلندترین قله جهان اورست است" value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>

                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer ">
                                        <div class="form-group row" style="margin-bottom: 0rem;">
                                            <label style="padding-top:5px;">پاسخ دانش آموز:</label>
                                            <p style="padding-top:5px;padding-right:10px">گزینه 2</p>
                                            <label style="padding-top:5px;padding-right:30px">پاسخ صحیح:</label>
                                            <p style="padding-top:5px;padding-right:10px">گزینه 3</p>
                                        </div>
                                    </div>
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <div class="info-box">

                            <div class="info-box-content">
                                <span class="info-box-text">نمره دانش آموز:</span>
                                <span class="info-box-number">98 از 100</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.content -->

@endsection
