@extends('panel.master')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <section class="col-lg-512 connectedSortable ui-sortable">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <div class="card bg-info-gradient collapsed-card">
                        <div class="card-header no-border ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                <i class="fa fa-comment-o mr-1"></i>
                                تابلو اعلانات مدرسه {{$school->name ?? ''}}
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul class="mr-4">
                                @foreach($school_bulletins as $school_bulletin)
                                    <li>{{$school_bulletin->content}}</li>
                                @endforeach
                            </ul>
                        </div>

                        <!-- /.card-footer -->
                    </div>

                    <div class="card bg-primary-gradient collapsed-card">
                        <div class="card-header no-border ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                <i class="fa fa-commenting-o mr-1"></i>
                                تابلو اعلانات مدرسه الکترونیکی یار مهربان
                            </h3>
                            <!-- card tools -->
                            <div class="card-tools">
                                <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"
                                        data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <div class="card-body ">
                            <ul class="mr-4">
                                @foreach($yare_mehraban_bulletins as $yare_mehraban_bulletin)
                                    <li>{{$yare_mehraban_bulletin->content}}</li>
                                @endforeach
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- Map card -->

        <!-- /.card -->

        <!-- solid sales graph -->

        <!-- /.card -->
        <div class="modal fade" id="modal-plus" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ثبت کار</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('job')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label>عنوان کار</label>
                                    <textarea id="content" name="comment" class="form-control" rows="3"
                                              placeholder="وارد کردن اطلاعات ..."></textarea>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>مدت زمان انجام</label>
                                        <input type="text" class="form-control" name="deadline"
                                               placeholder="مدت زمان انجام کار را وارد کنید">
                                    </div>

                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                                <button type="submit" class="btn btn-primary">ثبت</button>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-edit" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ویرایش کار</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('job')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label>عنوان کار</label>
                                    <textarea id="job-comment-edit" name="comment" class="form-control" rows="3"
                                              placeholder="وارد کردن اطلاعات ..."></textarea>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>مدت زمان انجام</label>
                                        <input id="job-deadline-edit" type="text" class="form-control" name="deadline"
                                               placeholder="مدت زمان انجام کار را وارد کنید">
                                    </div>
                                    <input id="job-id" type="hidden" class="form-control" name="id">
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                                <button type="submit" class="btn btn-primary">ثبت</button>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

    </section>
    <script>
        $(document).ready(function () {
            $('body').on('click', '#modal', function () {
                document.getElementById('job-comment-edit').innerHTML = $(this).attr('data-comment');
                document.getElementById('job-deadline-edit').value = $(this).attr('data-deadline');
                document.getElementById('job-id').value = $(this).attr('data-id');
            });
        });
    </script>

@endsection
