@extends('panel.master')
@section('content')

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">لیست پاسخ تکالیف</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست پاسخ‌ها</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>نمره</th>
                                            <th>تاریخ تخصیص</th>
                                            <th>پاسخ</th>
                                        </tr>
                                        @foreach($answers as $answer)
                                            <tr>
                                                <td>{{$answer->id}}</td>
                                                <td>{{$answer->student->user->name}} {{$answer->student->user->family}}</td>
                                                <td>{{$answer->grade}} از ۲۰</td>
                                                <td>{{$task->deadline}}</td>
                                                <td>
                                                    <button id="modal" type="button" class="btn btn-primary"
                                                            data-toggle="modal"
                                                            data-target="#modal-descriptive-check-feedback"
                                                            data-name="{{ $answer->student->user->name }}"
                                                            data-family="{{ $answer->student->user->family }}"
                                                            data-content="{{ $task->content }}"
                                                            data-answer="{{ $answer->answer }}"
                                                            data-grade="{{ $answer->grade }}"
                                                            data-comment="{{ $answer->comment_teacher }}"
                                                            data-id="{{ $answer->id }}"
                                                            data-file="{{ $answer->file_name }}">
                                                        <i class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <!-- .modal-default -->
        </div><!-- /.container-fluid -->


        <div class="modal fade" id="modal-descriptive-check-feedback" style="display: none;z-index:3001;"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="student-name"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning"
                                                  data-original-title="3 New Messages">3 نمره</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      id="question-content"
                                                      disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea id="answer-content" class="form-control" rows="3"
                                                      placeholder="وارد کردن اطلاعات ..."
                                                      disabled></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نمره</label>
                                                    <input name="grade" type="number" step="0.01" class="form-control"
                                                           id="grade-content" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نظر معلم</label>
                                                    <input disabled name="comment" type="text" class="form-control" id="comment-content">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <a id="download-link" class="btn btn-large pull-right"><i
                                                    class="icon-download-alt"> </i> دانلود پاسخ </a>
                                            <div class="form-group">
                                                <input name="id" type="hidden" class="form-control" id="edit-id">
                                            </div>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                            </div>
                         </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


    </div>
    <!-- /.content -->
    <script>
        $(document).ready(function () {
            $('body').on('click', '#modal', function () {
                document.getElementById('student-name').innerHTML = 'پاسخ تکلیف دانش آموز ' + $(this).attr('data-name') + ' ' + $(this).attr('data-family');
                document.getElementById('question-content').innerHTML = $(this).attr('data-content');
                document.getElementById('answer-content').innerHTML = $(this).attr('data-answer');
                document.getElementById('grade-content').value = $(this).attr('data-grade');
                document.getElementById('comment-content').value = $(this).attr('data-comment');
                document.getElementById('edit-id').value = $(this).attr('data-id');
                var file = $(this).attr('data-file')
                if (file) {
                    document.getElementById('download-link').href = 'download?id=' + $(this).attr('data-id')
                }
            });
        });
    </script>
@endsection
