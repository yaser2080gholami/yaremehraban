@extends('panel.master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">مشاهده تکلیف والدین</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست تکلیف</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام درس</th>
                                            <th>عنوان تکلیف</th>
                                            <th>تاریخ تخصیص</th>
                                            <th>مشاهده پاسخ</th>
                                        </tr>
                                        @foreach($tasks as $task)
                                            <tr>
                                                <td>{{$task->id}}</td>
                                                <td>{{$task->lesson_name}}</td>
                                                <td>{{$task->title}}</td>
                                                <td>{{$task->deadline}}</td>
                                                <td>
                                                    <a type="button" class="btn btn-warning" style="margin-top:5px;" href="{{route('parents.task.index-answer', ['id' => $task->id])}}" title="تصحیح تکلیف"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-descriptive-check" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">تکلیف</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning"
                                                  data-original-title="3 New Messages">x از y</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">تکلیف 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..."
                                                      disabled></textarea>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                                <span data-toggle="tooltip" title="" class="badge badge-warning"
                                                      data-original-title="3 New Messages">1 از 2</span>
                                            </button>
                                        </div>
                                        <h3 class="card-title" style="float:left;">تکلیف 2</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..."
                                                      disabled></textarea>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <div class="info-box">

                            <div class="info-box-content">
                                <span class="info-box-text">نمره تکلیف دانش آموز:</span>
                                <span class="info-box-number">17 از 20</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

    </div>
    <!-- /.content -->

@endsection
