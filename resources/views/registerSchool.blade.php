
@include('panel.frontend.css')
@include('panel.frontend.js')

<link rel="stylesheet" href="/css/hadi-css.css">
<!-- Register Add data css -->
<link rel="stylesheet" href="/dist/css/registerAddData.css">
<!-- Select2 -->
<link rel="stylesheet" href="/plugins/select2/select2.min.css">

<body class="login-page">
<div class="login-box">
    <div class="card haditopcardui">
        <div class="row pt-1">
            <div class="col-12 text-center">
                <img width="60px" src="/img/logo-yaremehraban.png">
            </div>
        </div>
        <div class="login-logo mb-0">
            <a href=""><b><span class="hadiwhitecolor hadititle" style="font-size:0.7em;">ثبت نام مدرسه</span></b></a>
        </div>
    </div>
    <!-- /.login-logo -->
    <div class="card hadicardmiddle">
        <div class="card-body login-card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <p>لطفا اطلاعات زیر را تکمیل و روی دکمه تایید، کلیک کنید.</p>
            <form action="{{route('infoSchool')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">

                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="name" placeholder="نام مدیر">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="family" placeholder="نام خانوادگی مدیر">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="schoolName" placeholder="نام مدرسه">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class=" mb-3">
                            {{--<select class="form-control" name="schoolEductionLevelList" placeholder="دوره های تحصیلی موجود در مدرسه" style="width: 100%;text-align: right">--}}
                                {{--@foreach($periods as $period)--}}
                                    {{--<option value="{{$period->id}}">{{$period->name}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            <select class="js-example-basic-multiple form-control" name="schoolEductionLevelList[]" placeholder="دوره های تحصیلی موجود در مدرسه"  multiple="multiple">
                                <option value=""     disabled>دوره تحصیلی</option>
                            @foreach($periods as $period)
                                    <option value="{{$period->id}}">{{$period->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class=" mb-3">
                            <input class="form-control" placeholder="تلفن همراه خود را وارد کنید" name="phone" id="registerProvince">

                            </input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class=" mb-3">
                            <input class="form-control" placeholder="کد ملی را وارد کنید" name="national_id" id="registerProvince">

                            </input>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class=" mb-3">
                            <input class="form-control" placeholder="استان را وارد کنید" name="state" id="registerProvince">

                            </input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class=" mb-3">
                            <input class="form-control" placeholder="شهر را وارد کنید" name="city" id="registerProvince">

                            </input>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="schoolPhoneNumber" placeholder="تلفن مدرسه">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <select class="form-control" name="schoolGender" placeholder="جنسیت">
                                <option value="" disabled selected> انتخاب جنسیت مدرسه</option>
                                <option value="">پسرانه</option>
                                <option value="">دخترانه</option>
                                <option value="">پسرانه-دخترانه</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class=" mb-3">
                            <input type="password" class="form-control" placeholder="رمز عبور" name="password" id="registerProvince">

                            </input>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class=" mb-3">
                            <input type="password" class="form-control" placeholder="تایید رمز عبور" name="password_confirmation" id="registerProvince">

                            </input>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="address" placeholder="آدرس مدرسه">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <textarea class="form-control" name="description" id="" cols="80" rows="5" placeholder="توضیحاتی درباره مدرسه بنویسید"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-6">
                        <p class="pt-2 text-center">
                            <a href="">بازگشت</a>
                        </p>
                    </div>
                    <div class="col-6">

                        <button type="submit" class="btn btn-primary btn-block btn-flat" style="        border-radius: 5px;" >تایید</button>
                    </div>
                    <div class="mt-3 col-12">

                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<script>
//    var changeProvinceAction = function(){
//        $.ajax({
//            url: getApiPrefixUrl() + "registerStudent/ajax/",
//            data: {
//                "Order": "getCity",
//                "idProvince": $("#registerProvince").val(),
//            },
//            cache: false,
//            async: false,
//            method: "GET",
//            success: function (response) {
//                var responseJson = JSON.parse(response);
//                if(responseJson.status != "ok"){
//                    alert("An error happen..."+responseJson.value);
//                    return;
//                }
//                var _html = "";
//                _html += `<option value="" disabled selected>انتخاب شهر</option>`;
//                for(let item of responseJson.object){
//                    _html += `<option value="${item.id}">${item.name}</option>`;
//                }
//                $("#schoolCity").html(_html);
//            },
//            error: function (XMLHttpRequest, textStatus, errorThrown) {
//                console.log(XMLHttpRequest);
//                console.log(textStatus);
//                console.log(errorThrown);
//                // Detect network failure.
//                alert("Error network failure :)");
//            }
//        });
//    }
</script>
<!-- iCheck -->
<script defer src="/plugins/iCheck/icheck.min.js"></script>
<!-- Select2 -->
<script defer src="/plugins/select2/select2.full.min.js"></script>
<script defer>
    $(function()
        $('.select2').select2();
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        })
    )
</script>


<script>

//    $(document).ready(function () {
//        var education=['ابتدایی','متوسط دوم','متوسط اول'];
//        $("#education").select2({
//            data:education
//        })
//    })
</script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
</script>

</script>
</body>
