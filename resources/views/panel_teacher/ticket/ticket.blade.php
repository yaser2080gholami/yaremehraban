@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">ارسال تیکت</h1>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{route('teacher.ticket.create')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="inputFirstname">نام</label>
                                <input name="name" type="text" class="form-control form-control-sm" id="inputFirstname"
                                       placeholder="نام" disabled=""
                                       value="{{\Illuminate\Support\Facades\Auth::user()->name.' '.\Illuminate\Support\Facades\Auth::user()->family}}">
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>اولیت</label>
                                    <select name="priority" class="form-control select2 select2-hidden-accessible"
                                            style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option value="1">کم</option>
                                        <option value="2" selected="selected">متوسط</option>
                                        <option value="3">زیاد</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>موضوع</label>
                                    <input type="text" class="form-control" name="subject">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>مقصد</label>
                                    <select id="state" onchange="loadDoc();" name="destination"
                                            class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                            tabindex="-1" aria-hidden="true">
                                        <option class="dest" value="teacher">معلم</option>
                                        <option class="dest" value="student">هم کلاسی</option>
                                        <option class="dest" value="manager">مدیر</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" style="display: none" id="dest_div">
                                    <label>نمایش</label>
                                    <select id="state2" name="user_id"
                                            class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                            tabindex="-1" aria-hidden="true">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>متن</label>
                            <textarea name="text" class="form-control" rows="3"
                                      placeholder="وارد کردن اطلاعات ..."></textarea>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="">
                                    <input name="file" type="file"  id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">انتخاب فایل</label>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">ارسال</button>

                            <a href="{{route('teacher.ticket.index')}}" class="btn btn-default">لغو</a>
                        </div>
                    </form>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <script>
        /*function loadDoc() {
            var state = document.getElementById("state").value;
            document.getElementById("dest_div").style.display = "inline-block"
            document.getElementById("dest_div").className = "form-group"
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var data = JSON.parse(xhttp.responseText);
                    var html = '';
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i]["user"]["id"] + '>' + data[i]["user"]["name"] + ' ' + data[i]["user"]["family"] + '</option>';
                    }
                    document.getElementById("state2").innerHTML = html;
                }
            };
            ``
            xhttp.open("GET", "/ticket/get-data", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("yaser=" + state);
        }*/
        function loadDoc() {
            document.getElementById("dest_div").style.display = "block"
            document.getElementById("dest_div").className = "form-group"
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var data = JSON.parse(xhttp.responseText);
                    var html = '';
                    console.log(data);
                    if ($("#state2").innerHTML !== '') {
                        $("#state2").children().remove();
                    }
                    for (i = 0; i < data.length; i++) {
                        $("#state2").append("<option value='" + data[i].user.id + "'>" + data[i].user.name + " " + data[i].user.family + "</option>");
                    }
                }
            };
            var id = $(".dest:selected").val();
            if(id === 'manager') {
                document.getElementById("dest_div").style.display = "none"
                document.getElementById("dest_div").className = "form-group"
            } else {
                xhttp.open("GET", "/ticket/get-data?state=" + id, true);
                xhttp.send();
            }
        }

    </script>
@endsection
