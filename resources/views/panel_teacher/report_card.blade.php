@extends('panel.master')
@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">کارنامه</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست کلاس ها</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>دوره و پایه</th>
                                            <th>نام درس</th>

                                            <th>شماره کلاس</th>
                                            <th>نام معلم</th>
                                            <th>مشاهده کارنامه</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>دوم متوسط - پایه اول</td>
                                            <td>ریاضی</td>
                                            <td>304</td>
                                            <td>علی علیان</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" title="مشاهده" data-target="#modal-view-homework"><i class="fa fa-eye"></i></button>
                                            </td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- modal-view-homework-->
        <div class="modal fade" id="modal-view-homework" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog" style="max-width:1000px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">مشاهده تکلیف دانش آموز</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام دانش آموز</th>
                                        <th>کد ملی</th>
                                        <th>کارنامه</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>علی علیان</td>
                                        <td>123456789</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" title="کارنامه" data-target="#modal-descriptive-check"><i class="fa fa-eye"></i></button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->

                        </form></div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- modal-descriptive-check-->
        <div class="modal fade" id="modal-descriptive-check" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog" style="min-width:1000px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">کارنامه</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="card-body" style="padding:4px;margin:0px 100px 0px 100px;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm">
                                                <label>نام :</label>
                                            </div>
                                            <div class="col-sm">
                                                <p>علی</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm"><label>نام خانوادگی :</label></div>
                                            <div class="col-sm">
                                                <p>علیان</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm">
                                                <label>کدملی :</label>
                                            </div>
                                            <div class="col-sm">
                                                <p>1234567895</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm"><label>نام پدر :</label></div>
                                            <div class="col-sm">
                                                <p>حسن علیان</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm">
                                                <label>شماره شناسنامه :</label>
                                            </div>
                                            <div class="col-sm">
                                                <p>456123</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm"><label>دوره و پایه :</label></div>
                                            <div class="col-sm">
                                                <p>دوره متوسطه اول پایه دوم</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm">
                                                <label>وضعیت جسمانی :</label>
                                            </div>
                                            <div class="col-sm">
                                                <p>سالم</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="row">
                                            <div class="col-sm"><label>سال تحصیلی :</label></div>
                                            <div class="col-sm">
                                                <p>1396</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-sm">
                                                <label>معدل کل :</label>
                                            </div>
                                            <div class="col-sm">
                                                <p>17.70</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border: 1px solid black;padding:4px;margin:0px 200px 0px 200px;">

                            <div class="row">
                                <div class="col-sm">
                                    <label>سال تحصیلی :</label>
                                </div>
                                <div class="col-sm">
                                    <p>1399-1400</p>
                                </div>
                            </div>

                        </div>
                        <div class="card-body" style="border: 1px solid black;padding:4px;margin:0px 200px 0px 200px;">
                            <table class="table compact dataTable">
                                <tbody>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام درس</th>
                                    <th>واحد نظری</th>
                                    <th>واحد عملی</th>
                                    <th>نمره</th>
                                    <th>نمره در ضریب</th>
                                    <th>وضعیت</th>
                                </tr>
                                <tr class="odd">
                                    <td>1</td>
                                    <td>ریاضی</td>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>20</td>
                                    <td>40</td>
                                    <td>پاس شده</td>
                                </tr>
                                <tr class="even">
                                    <td>1</td>
                                    <td></td>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>20</td>
                                    <td>40</td>
                                    <td>پاس شده</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>ریاضی</td>
                                    <td>1</td>
                                    <td>2</td>
                                    <td>20</td>
                                    <td>40</td>
                                    <td>پاس شده</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>


                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

    </div>
    <!-- /.content -->

@endsection
