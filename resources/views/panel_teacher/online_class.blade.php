@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">کلاس آنلاین</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default">
                                    <i class="fa fa-plus"></i>
                                    <a target="_blank" href="https://meet.google.com/">کلاس جدید</a>
                                </button>
                            </div>
                            <form action="{{route('teacher.online.create')}}" method="post">
                                @csrf
                                <label>کلاس:</label>
                                <select name="class_id" class="form-control select2 form-control-sm"
                                        style="width: 40%;">
                                    @foreach($classes as $class)
                                        {{$classes}}
                                        <option value="{{$class->id}}">{{$class->label}}</option>
                                    @endforeach
                                </select>
                                <br>
                                <div>
                                        به اشتراک گذاری لینک
                                        <input type='text' name='link' id="link">
                                        <button type="submit" class='btn btn-success'>ثبت</button>
                                </div>
                            </form>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
    </div>
    <!-- /.content -->
@endsection
