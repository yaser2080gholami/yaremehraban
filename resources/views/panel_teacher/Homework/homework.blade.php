@extends('panel.master')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">طرح آزمون</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default" data-toggle="modal"
                                        data-target="#modal-default">
                                    <i class="fa fa-plus"></i>
                                    آزمون جدید
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست آزمون</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام درس</th>
                                            <th>عنوان آزمون</th>
                                            <th>دوره و پایه</th>
                                            <th>تعداد سوال</th>
                                            <th>مدت زمان آزمون</th>
                                            <th>نوع آزمون</th>
                                            <th>تاریخ و زمان شروع</th>
                                            <th>طرح سوال</th>
                                            <th>تصحیح</th>
                                            <th>عملیات</th>
                                        </tr>
                                        @foreach($homeworks as $homework)
                                            <tr>
                                                <td>{{$homework->id}}</td>
                                                <td>{{$homework->lesson_name}}</td>
                                                <td>{{$homework->title}}</td>
                                                <td>{{$homework->course->name}}</td>
                                                <td>{{$homework->question_number}}</td>
                                                <td>{{$homework->expire}}</td>
                                                <td>{{$homework->type}}</td>
                                                <td>{{$homework->start_date}}</td>
                                                <td>
                                                    @if($homework->type === 'تستی')
                                                        <button type="button" class="btn btn-warning"
                                                                data-toggle="modal"
                                                                id="test-btn"
                                                                name="test-btn"
                                                                data-target="#modal-test-questions"
                                                                data-id="{{$homework->id}}"
                                                                title="طرح سوال تستی"><i
                                                                class="fa fa-check"></i></button>
                                                    @else
                                                        <button type="button" class="btn btn-warning"
                                                                style="margin-top:5px;"
                                                                id="discriptive-btn"
                                                                name="discriptive-btn"
                                                                data-toggle="modal"
                                                                data-id="{{$homework->id}}"
                                                                data-target="#modal-descriptive-questions"
                                                                title="طرح سوال تشریحی"><i class="fa fa-edit"></i>
                                                        </button>


                                                    @endif
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-warning"
                                                            style="margin-top:5px;"
                                                            data-toggle="modal" data-target="#modal-descriptive-check"
                                                            title="تصحیح آزمون تشریحی"><i class="fa fa-edit"></i>
                                                    </button>

                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                            id="edit-btn"
                                                            name="edit-btn"
                                                            data-target="#modal-edit" title="ویرایش"
                                                            data-id="{{$homework->id}}"
                                                            data-lesson-name="{{$homework->lesson_name}}"
                                                            data-title="{{$homework->title}}"
                                                            data-question-number="{{$homework->question_number}}"
                                                            data-course="{{$homework->course->name}}"
                                                            data-expire="{{$homework->expire}}"
                                                            data-type="{{$homework->type}}"
                                                            data-start="{{$homework->start_date}}"
                                                    ><i
                                                            class="fa fa-edit"></i></button>
                                                    <form style="display: inline" name="delete-form"
                                                          action="{{route('teacher.homework.delete')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$homework->id}}">
                                                        <button type="submit" class="btn btn-danger"
                                                                style="margin-top:5px;"><i
                                                                class="fa fa-trash" title="حذف"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <!-- .modal-default -->
        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-default" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">آزمون جدید</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" action="{{route('teacher.homework.create')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام درس</label>
                                        <input type="text" class="form-control form-control-sm" id="lesson_name"
                                               name="lesson_name" placeholder="نام درس">
                                    </div>
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label for="inputFirstname">عنوان آزمون</label>
                                            <input type="text" class="form-control form-control-sm" id="title"
                                                   name="title"
                                                   placeholder="عنوان آزمون">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label>دوره و پایه</label>
                                        <select name="course_id" id="course_id"
                                                class="form-control select2 form-control-sm select2-hidden-accessible"
                                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            @foreach($courses as $course)
                                                <option id="{{$course->name}}"
                                                        value="{{$course->id}}">{{$course->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputFirstname">تعداد سوال</label>
                                            <input type="number" class="form-control form-control-sm"
                                                   id="question_number" name="question_number" placeholder="تعداد سوال">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputFirstname">مدت زمان آزمون</label>
                                            <input type="number" class="form-control form-control-sm"
                                                   id="expire" name="expire" placeholder="دقیقه">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>نوع آزمون</label>
                                        <select id="type" name="type"
                                                class="form-control select2 form-control-sm select2-hidden-accessible"
                                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            <option value="تستی">تستی</option>
                                            <option value="تشریحی">تشریحی</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label>تاریخ شروع</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                        </span>
                                            </div>
                                            <input type="text"
                                                   class="normal-example form-control pwt-datepicker-input-element pdp-el"
                                                   id="start_date_date" name="start_date_date" pdp-id="pdp-7287967">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>زمان شروع</label>
                                            <input id="start_date_hour" name="start_date_hour"
                                                   class="form-control form-control-sm">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                                <button type="submit" class="btn btn-primary">ثبت</button>

                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-edit" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">آزمون جدید</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" action="{{route('teacher.homework.update')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام درس</label>
                                        <input type="text" class="form-control form-control-sm" id="edit_lesson_name"
                                               name="lesson_name" placeholder="نام درس">
                                    </div>
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label for="inputFirstname">عنوان آزمون</label>
                                            <input type="text" class="form-control form-control-sm" id="edit_title"
                                                   name="title"
                                                   placeholder="عنوان آزمون">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label>دوره و پایه</label>
                                        <select name="course_id" id="course_id"
                                                class="form-control select2 form-control-sm select2-hidden-accessible"
                                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            @foreach($courses as $course)
                                                <option id="{{$course->name}}"
                                                        value="{{$course->id}}">{{$course->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputFirstname">تعداد سوال</label>
                                            <input type="number" class="form-control form-control-sm"
                                                   id="edit_question_number" name="question_number"
                                                   placeholder="تعداد سوال">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputFirstname">مدت زمان آزمون</label>
                                            <input type="number" class="form-control form-control-sm"
                                                   id="edit_expire" name="expire" placeholder="دقیقه">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>نوع آزمون</label>
                                        <select id="type" name="type"
                                                class="form-control select2 form-control-sm select2-hidden-accessible"
                                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            <option id="تستی" value="تستی">تستی</option>
                                            <option id="تشریحی" value="تشریحی">تشریحی</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label>تاریخ شروع</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                        </span>
                                            </div>
                                            <input type="text"
                                                   class="normal-example form-control pwt-datepicker-input-element pdp-el"
                                                   id="edit_start_date_date" name="start_date_date"
                                                   pdp-id="pdp-7287967">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>زمان شروع</label>
                                            <input id="edit_start_date_hour" name="start_date_hour"
                                                   class="form-control form-control-sm">
                                        </div>
                                        <input type="hidden" class="form-control form-control-sm"
                                               id="edit_id" name="id">

                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                                <button type="submit" class="btn btn-primary">ثبت</button>

                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-descriptive-questions" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">طرح سوال آزمون تشریحی</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="{{route('teacher.question.add')}}" method="post">
                        @csrf
                        <input type="hidden" name="type" id="type" value="descriptive">
                        <input type="hidden" name="homework_id" id="homework_id_descriptive">
                        <div class="modal-body">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="form-control"
                                 style="border:1px solid #ced4da;padding:10px;margin-bottom: 10px;">
                                <div class="mx-auto" style="width: 100%;margin:0px;">
                                    <div class="form-group">
                                        <label>سوال</label>
                                        <textarea id="title" name="title" class="form-control" rows="3"
                                                  placeholder="لطفا سوال را وارد کنید"></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">

                                            <input name="grade" id="grade" type="text" class="form-control col-3" placeholder="بارم">

                                        </div>
                                        <div class="col-sm-6">

                                            <button type="submit" class="btn btn-danger">اضافه کردن</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body table-responsive p-0">
                                <div class="form-control" style="border:1px solid #ced4da;padding:10px;">
                                    <label>لیست سوال ها</label>
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>سوال</th>
                                            <th>بارم</th>
                                            <th>عملیات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی علیان</td>
                                            <td>1234567899</td>
                                            <td>
                                                <button type="button" class="btn btn-success"><i class="fa fa-edit"
                                                                                                 title="ویرایش"></i>
                                                </button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"
                                                                                                title="حذف"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card-body -->

                    </div>
                    </form>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>


                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-test-questions" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">طرح سوال آزمون تستی</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <!-- /.card-header -->
                            <form action="{{route('teacher.question.add')}}" method="post">
                                @csrf
                                <input type="hidden" name="type" id="type" value="test">
                                <input type="hidden" name="homework_id" id="homework_id_test">
                                <div class="form-control"
                                     style="border:1px solid #ced4da;padding:10px;margin-bottom: 10px;">
                                    <div class="mx-auto" style="width: 100%;margin:0px;">
                                        <div class="form-group">
                                            <label>سوال</label>
                                            <textarea name="title" id="title" class="form-control" rows="3"
                                                      placeholder="لطفا سوال را وارد کنید"></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6" style="padding-top:5px;">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="radio" name="correct_option" value="1"></span>
                                                    </div>
                                                    <input name="option1" id="option1" type="text" class="form-control"
                                                           placeholder="گزینه 1">
                                                </div>
                                            </div>
                                            <div class="col-sm-6" style="padding-top:5px;">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="radio" name="correct_option" value="2"></span>
                                                    </div>
                                                    <input type="text" name="option2" id="option2" class="form-control"
                                                           placeholder="گزینه 2">
                                                </div>


                                            </div>
                                            <div class="col-sm-6" style="padding-top:5px;">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="radio" name="correct_option" value="3"></span>
                                                    </div>
                                                    <input type="text" name="option3" id="option3" class="form-control"
                                                           placeholder="گزینه 3">
                                                </div>


                                            </div>
                                            <div class="col-sm-6" style="padding-top:5px;">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <input type="radio" name="correct_option" value="4"></span>
                                                    </div>
                                                    <input type="text" name="option4" id="option4" class="form-control"
                                                           placeholder="گزینه 4">
                                                </div>


                                            </div>

                                        </div>

                                        <button type="submit" class="btn btn-danger float-left">اضافه کردن</button>


                                    </div>
                                </div>
                            </form>
                            <div class="card-body table-responsive p-0">
                                <div class="form-control" style="border:1px solid #ced4da;padding:10px;">
                                    <label>لیست سوال ها</label>
                                    <table class="table table-hover" style="font-size: 0.7em;">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>سوال</th>
                                            <th>گزینه 1</th>
                                            <th>گزینه 2</th>
                                            <th>گزینه 3</th>
                                            <th>گزینه 4</th>
                                            <th>عملیات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>لورم اپاسیوم متنی</td>
                                            <td>الف</td>
                                            <td>ب</td>
                                            <td>ج</td>
                                            <td>د</td>
                                            <td>
                                                <button type="button" class="btn btn-success"><i class="fa fa-edit"
                                                                                                 title="ویرایش"></i>
                                                </button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"
                                                                                                title="حذف"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت</button>


                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        </div>

        <div class="modal fade" id="modal-descriptive-check" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">لیست برگه های پاسخنامه آزمون تشریحی کلاس 304</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">

                            <div class="card-body table-responsive p-0">
                                <div class="form-control" style="border:1px solid #ced4da;padding:10px;">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>نمره</th>
                                            <th>عملیات تصحیح</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی علیان</td>
                                            <td>19 از 20</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#modal-descriptive-check-feedback" title="ویرایش">
                                                    <i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-test-check" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">لیست نمرات آزمون تستی کلاس 302</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">

                            <div class="card-body table-responsive p-0">
                                <div class="form-control" style="border:1px solid #ced4da;padding:10px;">
                                    <label>لیست دانش آموزان کلاس عربی</label>
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>نمره</th>
                                            <th>توضیحات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی علیان</td>
                                            <td>19 از 20</td>
                                            <td>
                                                خوب! افرین

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-descriptive-check-feedback" style="display: none;z-index:3001;"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">برگه پاسخنامه دانش آموز: محمد اناری</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning"
                                                  data-original-title="3 New Messages">3 نمره</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled=""></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..."
                                                      disabled=""></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نمره</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="نمره">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نظر معلم</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="نمره">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning"
                                                  data-original-title="3 New Messages">3 نمره</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 2</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled=""></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..."
                                                      disabled=""></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نمره</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="نمره">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نظر معلم</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="نمره">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->

                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت نهایی</button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.content -->
    <script>
        $(document).ready(function () {
            $('body').on('click', '#edit-btn', function () {
                document.getElementById('edit_lesson_name').value = $(this).attr('data-lesson-name');
                document.getElementById('edit_title').value = $(this).attr('data-title');
                document.getElementById('edit_id').value = $(this).attr('data-id');
                document.getElementById('edit_expire').value = $(this).attr('data-expire');
                document.getElementById('edit_question_number').value = $(this).attr('data-question-number');
                document.getElementById($(this).attr('data-course')).selected = true;
                document.getElementById($(this).attr('data-type')).selected = true;
                var date = $(this).attr('data-start');
                var splitted = date.split(' ');
                document.getElementById('edit_start_date_date').value = splitted[0];
                document.getElementById('edit_start_date_hour').value = splitted[1];
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('body').on('click', '#test-btn', function () {
                console.log($(this).attr('data-id'));
                document.getElementById('homework_id_test').value = $(this).attr('data-id');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('body').on('click', '#discriptive-btn', function () {
                document.getElementById('homework_id_descriptive').value = $(this).attr('data-id');
            });
        });
    </script>

@endsection
