@extends('panel.master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">مورد انضباطی</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                                    <i class="fa fa-plus"></i>
                                    مورد انضباطی جدید
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست موارد</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>

                                            <th>تاریخ</th>
                                            <th>دوره و پایه</th>
                                            <th>کلاس</th>
                                            <th>نام دانش آموز</th>

                                            <th>عنوان انضباطی</th>
                                            <th>مقدار کسر نمره</th>
                                            <th>توضیحات</th>
                                            <th>عملیات</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>

                                            <td>98/11/24</td>
                                            <td>متوسطه اول - پایه دوم</td>
                                            <td>306</td>
                                            <td>علی هلیان</td>

                                            <td>تاخیر</td>
                                            <td>1</td>
                                            <td>مورد نمونه</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>

                                            <td>98/12/23</td>
                                            <td>متوسطه اول - پایه دوم</td>
                                            <td>306</td>
                                            <td>علی شسیس</td>

                                            <td>تاخیر</td>
                                            <td>1</td>
                                            <td>مورد نمونه</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- modal-default -->
        <div class="modal fade" id="modal-default" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 750px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">مورد انضباطی جدید</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">تاریخ</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                        </span>
                                            </div>
                                            <input class="normal-example form-control pwt-datepicker-input-element" id='input2'>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">دوره و پایه</label>
                                        <select class="form-control select2 form-control-sm" data-placeholder="یک استان انتخاب کنید" style="width: 100%;text-align: right">
                                            <option>دوره متوسطه اول - پایه دوم</option>
                                            <option>دوره متوسطه دوم - پایه دوم</option>
                                            <option>دوره متوسطه اول - پایه سوم</option>
                                            <option>دوره ابتدایی - پایه دوم</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">شماره کلاس</label>
                                        <input type="number" class="form-control form-control-sm" id="inputFirstname" placeholder="شماره کلاس">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">نام دانش آموز</label>
                                        <select class="form-control select2 form-control-sm" data-placeholder="یک استان انتخاب کنید" style="width: 100%;text-align: right">
                                            <option>علی هلیان</option>
                                            <option>حسن حسنی</option>
                                            <option>سوم سومی</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <div class="col-sm-6">
                                        <label for="inputFirstname">عنوان انضباطی</label>
                                        <select class="form-control select2 form-control-sm" multiple="multiple" data-placeholder="یک استان انتخاب کنید" style="width: 100%;text-align: right">
                                            <option>تاخیر</option>
                                            <option>غیبت</option>
                                            <option>هرچیز</option>
                                            <option>هیچ چیز</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">مقدار کسر نمره</label>
                                        <input type="number" class="form-control form-control-sm" id="inputLastname" placeholder="مقدار کسر نمره">
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <label for="inputLastname">توضیحات</label>
                                    <textarea class="form-control" rows="2" placeholder="وارد کردن اطلاعات ..." style="resize: none;"></textarea>

                                </div>
                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.content -->
@endsection
