@extends('panel.master')
@section('content')

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">طرح تکلیف</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default" data-toggle="modal"
                                        data-target="#modal-default">
                                    <i class="fa fa-plus"></i>
                                    تکلیف جدید
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست تکالیف</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام درس</th>
                                            <th>عنوان تکلیف</th>
                                            <th>دوره و پایه</th>
                                            <th>زمان تحویل</th>
                                            <th>تخصیص به کلاس</th>
                                            <th>سوال</th>
                                            <th>تصحیح</th>
                                            <th>عملیات</th>
                                        </tr>
                                        @foreach($task as $tsk)
                                            <tr>
                                                <td>{{$tsk->id}}</td>
                                                <td>{{$tsk->lesson_name}}</td>
                                                <td>{{$tsk->title}}</td>
                                                <td>{{$tsk->school_class->course->name}}</td>
                                                <td>{{$tsk->deadline}}</td>
                                                <td>{{$tsk->school_class->label}}</td>
                                                <td>{{$tsk->content}}</td>
                                                <td>
                                                    <a type="button" class="btn btn-warning"
                                                            style="margin-top:5px;"
                                                            href="{{route('teacher.task.correct-index', ['id' => $tsk->id])}}"
                                                            title="تصحیح تکلیف"><i class="fa fa-edit"></i></a>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary modal-edit"
                                                            data-toggle="modal"
                                                            data-target="#modal-edit" title="ویرایش"
                                                            data-id="{{ $tsk->id }}"
                                                            data-title="{{ $tsk->title }}"
                                                            data-content="{{ $tsk->content }}"
                                                            data-class="{{ $tsk->school_class->label }}"
                                                            data-course="{{ $tsk->school_class->course->name }}"
                                                            data-teacher="{{ $tsk->teacher_id }}"
                                                            data-deadline="{{ $tsk->deadline }}"
                                                            data-lesson_name="{{ $tsk->lesson_name }}">
                                                        <i
                                                            class="fa fa-edit"></i></button>

                                                    <button type="submit" form="delete-form" class="btn btn-danger"><i class="fa fa-trash"
                                                                                                                       title="حذف"></i>
                                                    </button>
                                                    <form id="delete-form" action="{{route('teacher.task.delete')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$tsk->id}}">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <!-- .modal-default -->
        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-edit" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ویرایش تکلیف</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('teacher.task.update')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام درس</label>
                                        <input name="lesson_name" type="text" class="form-control form-control-sm"
                                               id="lesson_name" placeholder="نام درس" value="">
                                    </div>
                                    <input name="id" type="hidden" class="form-control form-control-sm"
                                           id="id">
                                    <input name="teacher_id" type="hidden" class="form-control form-control-sm"
                                           id="teacher_id">
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label for="inputFirstname">عنوان تکلیف</label>
                                            <input name="title" type="text" class="form-control form-control-sm"
                                                   id="title" placeholder="عنوان تکلیف">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label>دوره و پایه</label>
                                        <select name="course_id" class="form-control select2 form-control-sm"
                                                id="course_id" style="width: 100%;">
                                            @foreach($teacher->school_class as $course)
                                                <p></p>
                                                <option id="{{$course->course->name}}" value="{{$course->course->id}}"
                                                >{{$course->course->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>تاریخ تحویل</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                   <span class="input-group-text">
                                                     <i class="fa fa-calendar"></i>
                                                   </span>
                                                </div>
                                                <input name="deadline-date" type="text"
                                                       class="normal-example form-control pwt-datepicker-input-element"
                                                       id="deadline-date"/>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>زمان تحویل</label>
                                            <input name="deadline-hour"  class="form-control form-control-sm" id="deadline-hour"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>تخصیص به کلاس</label>
                                        <select name="class_id" class="form-control select2 form-control-sm"
                                                style="width: 100%;">
                                            @foreach($teacher->school_class as $class)
                                                <option id="{{$class->label}}" value="{{$class->id}}"
                                                >{{$class->label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>طرح سوال</label>
                                    <textarea id="content" name="content" class="form-control" rows="3"
                                              placeholder="وارد کردن اطلاعات ..."></textarea>
                                </div>


                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <div class="modal fade" id="modal-default" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">تکلیف جدید</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('teacher.task.create')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام درس</label>
                                        <input name="lesson_name" type="text" class="form-control form-control-sm"
                                               id="inputFirstname" placeholder="نام درس">
                                    </div>
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label for="inputFirstname">عنوان تکلیف</label>
                                            <input name="title" type="text" class="form-control form-control-sm"
                                                   id="inputFirstname" placeholder="عنوان تکلیف">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label>دوره و پایه</label>
                                        <select name="course_id" class="form-control select2 form-control-sm"
                                                style="width: 100%;">
                                            {{--                                            <option selected="selected">ابتدایی پایه اول</option>--}}
                                            @foreach($teacher->school_class as $course)
                                                <p></p>
                                                <option value="{{$course->course->id}}"
                                                        selected="selected">{{$course->course->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>تاریخ تحویل</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-calendar"></i>
                          </span>
                                                </div>
                                                <input name="deadline-date" type="text"
                                                       class="normal-example form-control pwt-datepicker-input-element"
                                                       id="input1"/>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>زمان تحویل</label>
                                            <input name="deadline-hour" class="form-control form-control-sm" id="demo-input2"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>تخصیص به کلاس</label>
                                        <select name="class_id" class="form-control select2 form-control-sm"
                                                style="width: 100%;">
                                            @foreach($teacher->school_class as $class)
                                                <option value="{{$class->id}}"
                                                        selected="selected">{{$class->label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>طرح سوال</label>
                                    <textarea name="content" class="form-control" rows="3"
                                              placeholder="وارد کردن اطلاعات ..."></textarea>
                                </div>


                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-descriptive-check" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">لیست تکالیف کلاس 403</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">

                            <div class="card-body table-responsive p-0">
                                <div class="form-control" style="border:1px solid #ced4da;padding:10px;">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>نمره</th>
                                            <th>عملیات تصحیح</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>علی علیان</td>
                                            <td>19 از 20</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#modal-descriptive-check-feedback" title="ویرایش">
                                                    <i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-descriptive-check-feedback" style="display: none;z-index:3001;"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">پاسخ تکلیف دانش آموز : محمد اناری</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="card-body">
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning"
                                                  data-original-title="3 New Messages">3 نمره</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 1</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..."
                                                      disabled></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نمره</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="نمره">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نظر معلم</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                                <div class="card collapsed-card">
                                    <div class="card-header ui-sortable-handle" style="cursor: move;">


                                        <div class="card-tools" style="left:auto;right:1rem;">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <span data-toggle="tooltip" title="" class="badge badge-warning"
                                                  data-original-title="3 New Messages">3 نمره</span>
                                        </div>
                                        <h3 class="card-title" style="float:left;">سوال 2</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      placeholder="بلندترین قله جهان اورست است"
                                                      value="بلندترین قله جهان اورست است" disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>پاسخ دانش آموز</label>
                                            <textarea class="form-control" rows="3" placeholder="وارد کردن اطلاعات ..."
                                                      disabled></textarea>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نمره</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="نمره">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">نظر معلم</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="نمره">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.direct-chat-pane -->
                                    </div>
                                    <!-- /.card-body -->
                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.card-body -->

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت نهایی</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.content -->
    <script>
        $(document).ready(function () {
            $('body').on('click', '.modal-edit', function () {
                document.getElementById('lesson_name').value = $(this).attr('data-lesson_name');
                document.getElementById('title').value = $(this).attr('data-title');
                document.getElementById('content').value = $(this).attr('data-content');
                document.getElementById('id').value = $(this).attr('data-id');
                document.getElementById('teacher_id').value = $(this).attr('data-teacher');
                document.getElementById($(this).attr('data-course')).selected = true;
                document.getElementById($(this).attr('data-class')).selected = true;
                var date = $(this).attr('data-deadline');
                console.log($(this).attr('data-class'));
                var splitted = date.split(' ');
                document.getElementById('deadline-date').value = splitted[0];
                document.getElementById('deadline-hour').value = splitted[1];
            });
        });
    </script>
@endsection
