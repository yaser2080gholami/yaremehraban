@include('panel.frontend.css')
@include('panel.frontend.js')
<body class="login-page">
<link rel="stylesheet" href="/css/hadi-css.css">
<div class="login-box">
    <div class="card haditopcardui">
        <div class="row pt-1">
            <div class="col-12 text-center">
                <img width="60px" src="/img/logo-yaremehraban.png">
            </div>
        </div>
        <div class="login-logo mb-0">
            <a href="../../index2.html"><b><span class="hadiwhitecolor hadititle" style="font-size:0.7em;">ثبت نام دانش آموز</span></b></a>
        </div>
    </div>
    <!-- /.login-logo -->
    <div class="card rounded-0 hadicardmiddle">
        <div class="card-body login-card-body">
            <p>لطفا کد پیگیری ارسال شده به شماره موبایل
                {{session()->get('phone')}}
                را وارد نمایید.</p>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('postverify')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <input type="number" class="form-control" name="token" placeholder="کد ارسال شده">
                        </div>
                        <div class="row">
                            <div id="spinner" class="col-0 p-0 m-0 text-left" style="display:none;"><div class="lds-dual-ring"></div></div>
                            <div class="col-12 p-0 m-0">
                                <p style="font-size: .7em; margin-top:8px;">
                                    <a href="{{route('resend-code')}}" id="sendagain" class="hadibold" style="padding-right:35px;">ارسال مجدد</a>
                                </p>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" style="border-radius: 5px;">تایید</button>
                    </div>
                </div>
            </form>
        </div>
    <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    @media only screen and (min-width: 768px) {
        .login-box {
            width: 700px;
        }
    }
</style>


<!-- jQuery -->
<script defer src="/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script defer src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script defer src="/plugins/iCheck/icheck.min.js"></script>
<script defer src="/dist/js/step-form.js"></script>
<script>
    $(function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        })
    })
</script>
</body>
