@extends('panel.master')
@section('content')

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">لیست تکالیف</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست تکالیف</h3>
                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام کلاس</th>
                                            <th>نام معلم</th>
                                            <th>نام درس</th>
                                            <th>عنوان تکلیف</th>
                                            <th>دوره و پایه</th>
                                            <th>مشاهده پاسخ‌ها</th>
                                        </tr>
                                        @foreach($tasks as $task)
                                            <tr>
                                                <td>{{$task->id}}</td>
                                                <td>{{$task->school_class->label}}</td>
                                                <td>{{$task->teacher->user->name}} {{$task->teacher->user->family}}</td>
                                                <td>{{$task->lesson_name}}</td>
                                                <td>{{$task->title}}</td>
                                                <td>{{$task->school_class->course->name}}</td>
                                                <td>
                                                    <a type="button" class="btn btn-warning" style="margin-top:5px;" href="{{route('manager.task.index-answer', ['id' => $task->id])}}" title="تصحیح تکلیف"><i class="fa fa-edit"></i></a>
                                                </td>

{{--                                                <td>--}}
{{--                                                    <a type="button" class="btn btn-warning"--}}
{{--                                                            style="margin-top:5px;"--}}
{{--                                                            href="{{route('teacher.task.correct-index', ['id' => $tsk->id])}}"--}}
{{--                                                            title="تصحیح تکلیف"><i class="fa fa-edit"></i></a>--}}
{{--                                                </td>--}}
{{--                                                <td>--}}
{{--                                                    <button type="submit" form="delete-form" class="btn btn-danger"><i class="fa fa-trash"--}}
{{--                                                                                                                       title="حذف"></i>--}}
{{--                                                    </button>--}}
{{--                                                    <form id="delete-form" action="{{route('teacher.task.delete')}}" method="post">--}}
{{--                                                        @csrf--}}
{{--                                                        <input type="hidden" name="id" value="{{$tsk->id}}">--}}
{{--                                                    </form>--}}
{{--                                                </td>--}}
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->

    </div>
@endsection
