@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">تعریف کلاس</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default" data-toggle="modal"
                                        data-target="#modal-default">
                                    <i class="fa fa-plus"></i>
                                    کلاس جدید
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست کلاس</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body table-responsive p-0">
                                        <table class="table table-hover">
                                            <tbody>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>دوره و پایه</th>
                                                <th>شماره کلاس</th>
                                                <th>ظرفیت</th>
                                                <th>پیکربندی کلاس</th>
                                                <th>اضافه کردن دانش آموز</th>
                                                <th>عملیات</th>
                                            </tr>
                                            @foreach($classes as $class)
                                                <tr>
                                                    <td id="hdClassID">{{$class->id}}</td>
                                                    <td>{{$class->course->name}}</td>
                                                    <td>{{$class->label}}</td>
                                                    <td>{{$class->capacity}}</td>
                                                    <td>
                                                        <button type="button" id="button" class="btn btn-warning"
                                                                data-toggle="modal" data-target="#modal-config"
                                                                title="پیکربندی کلاس"><i class="fa fa-cogs"></i>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-success"
                                                                data-toggle="modal" data-target="#modal-student"
                                                                name="add-modal"
                                                                id="hdBTNClass"
                                                                data-class-id="{{$class->id}}"
                                                                data-class="{{$class->student}}"
                                                                title="اضافه کردن دانش آموز" onclick="getListofStudents()"><i class="fa fa-user"></i>
                                                        </button>
                                                    </td>

                                                    <td>
                                                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default" title="ویرایش"><i class="fa fa-edit"></i></button> -->
                                                        <form action="{{route('manager.class.delete')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$class->id}}">
                                                            <button type="submit" class="btn btn-danger"><i
                                                                    class="fa fa-trash" title="حذف"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- /.card-body -->
                                </div>
                                <div class="mx-auto" style="width: 200px;margin:20px;">
                                    <button type="button" class="btn btn-default" data-toggle="modal"
                                            data-target="#modal-default">
                                        اتمام پیکره بندی
                                    </button>
                                </div>
                                <!-- /.card -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- modal-default -->
            <div class="modal fade" id="modal-default" style="display: none;z-index:3001;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">کلاس جدید</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{route('manager.class.create')}}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>دوره و پایه</label>
                                        <select name="course_id" id="course_id" class="form-control"
                                                style="width: 100%;">
                                            @foreach($courses as $course)
                                                <option value="{{$course->id}}">{{$course->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-sm-6">
                                            <label for="inputFirstname">نام کلاس</label>
                                            <input name="class_name" type="tezt" class="form-control form-control-sm"
                                                   id="class-name" placeholder="نام کلاس">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="inputLastname">ظرفیت کلاس</label>
                                            <input name="capacity" type="number"
                                                   class="form-control form-control-sm" id="capacity"
                                                   placeholder="چند نفر؟">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>انتخاب معلم</label>
                                        <select name="teacher_id" class="form-control"
                                                style="width: 100%;text-align: right">
                                            @foreach($teachers as $teacher)
                                                <option
                                                    value="{{$teacher->teacher_id}}">{{$teacher->user->name}} {{$teacher->user->family}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>
                                <!-- /.card-body -->
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                                    <button type="submit" class="btn btn-primary">ثبت</button>

                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <!-- modal-config -->
            <div class="modal fade" id="modal-config" style="display: none;z-index:3001;" aria-hidden="true">
                <div class="modal-dialog" style="min-width:1000px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">پیکره بندی</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div id="calendar" class="fc fc-media-screen fc-direction-rtl fc-theme-standard">
                                <div class="fc-header-toolbar fc-toolbar fc-toolbar-ltr">
                                    <div class="fc-toolbar-chunk">
                                        <div class="fc-button-group">
                                            <button class="fc-prev-button fc-button fc-button-primary" type="button"
                                                    aria-label="prev"><span
                                                    class="fc-icon fc-icon-chevron-right"></span></button>
                                            <button class="fc-next-button fc-button fc-button-primary" type="button"
                                                    aria-label="next"><span class="fc-icon fc-icon-chevron-left"></span>
                                            </button>
                                        </div>
                                        <button disabled="" class="fc-today-button fc-button fc-button-primary"
                                                type="button">امروز
                                        </button>
                                    </div>
                                    <div class="fc-toolbar-chunk"><h2 class="fc-toolbar-title">۱۳۹۹ مرداد – ۱۳۹۹
                                            شهریور</h2></div>
                                    <div class="fc-toolbar-chunk">
                                        <div class="fc-button-group">
                                            <button
                                                class="fc-dayGridMonth-button fc-button fc-button-primary fc-button-active"
                                                type="button">ماه
                                            </button>
                                            <button class="fc-timeGridWeek-button fc-button fc-button-primary"
                                                    type="button">هفته
                                            </button>
                                            <button class="fc-timeGridDay-button fc-button fc-button-primary"
                                                    type="button">روز
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="fc-view-harness fc-view-harness-active" style="padding-bottom: 74.0741%;">
                                    <div class="fc-daygrid fc-dayGridMonth-view fc-view">
                                        <table class="fc-scrollgrid  fc-scrollgrid-liquid">
                                            <thead>
                                            <tr class="fc-scrollgrid-section ">
                                                <td>
                                                    <div class="fc-scroller-harness">
                                                        <div class="fc-scroller" style="overflow: hidden;">
                                                            <table class="fc-col-header " style="width: 0px;">
                                                                <colgroup></colgroup>
                                                                <tbody>
                                                                <tr>
                                                                    <th class="fc-col-header-cell fc-day fc-day-sat">
                                                                        <div class="fc-scrollgrid-sync-inner"><a
                                                                                class="fc-col-header-cell-cushion ">شنبه</a>
                                                                        </div>
                                                                    </th>
                                                                    <th class="fc-col-header-cell fc-day fc-day-sun">
                                                                        <div class="fc-scrollgrid-sync-inner"><a
                                                                                class="fc-col-header-cell-cushion ">یکشنبه</a>
                                                                        </div>
                                                                    </th>
                                                                    <th class="fc-col-header-cell fc-day fc-day-mon">
                                                                        <div class="fc-scrollgrid-sync-inner"><a
                                                                                class="fc-col-header-cell-cushion ">دوشنبه</a>
                                                                        </div>
                                                                    </th>
                                                                    <th class="fc-col-header-cell fc-day fc-day-tue">
                                                                        <div class="fc-scrollgrid-sync-inner"><a
                                                                                class="fc-col-header-cell-cushion ">سه‌شنبه</a>
                                                                        </div>
                                                                    </th>
                                                                    <th class="fc-col-header-cell fc-day fc-day-wed">
                                                                        <div class="fc-scrollgrid-sync-inner"><a
                                                                                class="fc-col-header-cell-cushion ">چهارشنبه</a>
                                                                        </div>
                                                                    </th>
                                                                    <th class="fc-col-header-cell fc-day fc-day-thu">
                                                                        <div class="fc-scrollgrid-sync-inner"><a
                                                                                class="fc-col-header-cell-cushion ">پنجشنبه</a>
                                                                        </div>
                                                                    </th>
                                                                    <th class="fc-col-header-cell fc-day fc-day-fri">
                                                                        <div class="fc-scrollgrid-sync-inner"><a
                                                                                class="fc-col-header-cell-cushion ">جمعه</a>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="fc-scrollgrid-section  fc-scrollgrid-section-liquid">
                                                <td>
                                                    <div class="fc-scroller-harness fc-scroller-harness-liquid">
                                                        <div class="fc-scroller fc-scroller-liquid-absolute"
                                                             style="overflow: hidden auto;">
                                                            <div class="fc-daygrid-body fc-daygrid-body-unbalanced "
                                                                 style="width: 0px;">
                                                                <table class="fc-scrollgrid-sync-table"
                                                                       style="width: 0px; height: 0px;">
                                                                    <colgroup></colgroup>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past"
                                                                            data-date="2020-08-01">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۱</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                            data-date="2020-08-02">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۲</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past"
                                                                            data-date="2020-08-03">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۳</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-tue fc-day-past"
                                                                            data-date="2020-08-04">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۴</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-wed fc-day-past"
                                                                            data-date="2020-08-05">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۵</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-thu fc-day-past"
                                                                            data-date="2020-08-06">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۶</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-fri fc-day-past"
                                                                            data-date="2020-08-07">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۷</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past"
                                                                            data-date="2020-08-08">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۸</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                            data-date="2020-08-09">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۹</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past"
                                                                            data-date="2020-08-10">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۰</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-tue fc-day-past"
                                                                            data-date="2020-08-11">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۱</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-wed fc-day-past"
                                                                            data-date="2020-08-12">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۲</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-thu fc-day-past"
                                                                            data-date="2020-08-13">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۳</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-fri fc-day-past"
                                                                            data-date="2020-08-14">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۴</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past"
                                                                            data-date="2020-08-15">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۵</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                            data-date="2020-08-16">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۶</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past"
                                                                            data-date="2020-08-17">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۷</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-tue fc-day-today "
                                                                            data-date="2020-08-18">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۸</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-wed fc-day-future"
                                                                            data-date="2020-08-19">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۹</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-thu fc-day-future"
                                                                            data-date="2020-08-20">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۳۰</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-fri fc-day-future"
                                                                            data-date="2020-08-21">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۳۱</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sat fc-day-future"
                                                                            data-date="2020-08-22">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sun fc-day-future"
                                                                            data-date="2020-08-23">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-mon fc-day-future"
                                                                            data-date="2020-08-24">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۳</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-tue fc-day-future"
                                                                            data-date="2020-08-25">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۴</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-wed fc-day-future"
                                                                            data-date="2020-08-26">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۵</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-thu fc-day-future"
                                                                            data-date="2020-08-27">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۶</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-fri fc-day-future"
                                                                            data-date="2020-08-28">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۷</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sat fc-day-future"
                                                                            data-date="2020-08-29">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۸</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sun fc-day-future"
                                                                            data-date="2020-08-30">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۹</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-mon fc-day-future"
                                                                            data-date="2020-08-31">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۰</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-tue fc-day-future fc-day-other"
                                                                            data-date="2020-09-01">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۱</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-wed fc-day-future fc-day-other"
                                                                            data-date="2020-09-02">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۲</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-thu fc-day-future fc-day-other"
                                                                            data-date="2020-09-03">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۳</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-fri fc-day-future fc-day-other"
                                                                            data-date="2020-09-04">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۴</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sat fc-day-future fc-day-other"
                                                                            data-date="2020-09-05">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۵</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-sun fc-day-future fc-day-other"
                                                                            data-date="2020-09-06">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۶</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-mon fc-day-future fc-day-other"
                                                                            data-date="2020-09-07">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۷</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-tue fc-day-future fc-day-other"
                                                                            data-date="2020-09-08">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۸</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-wed fc-day-future fc-day-other"
                                                                            data-date="2020-09-09">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۱۹</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-thu fc-day-future fc-day-other"
                                                                            data-date="2020-09-10">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۰</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fc-daygrid-day fc-day fc-day-fri fc-day-future fc-day-other"
                                                                            data-date="2020-09-11">
                                                                            <div
                                                                                class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                <div class="fc-daygrid-day-top"><a
                                                                                        class="fc-daygrid-day-number">۲۱</a>
                                                                                </div>
                                                                                <div
                                                                                    class="fc-daygrid-day-events"></div>
                                                                                <div class="fc-daygrid-day-bg"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                            <button type="submit" class="btn btn-primary">ثبت</button>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>

            <!-- modal-student -->
            <div class="modal fade" id="modal-student" style="display: none;z-index:3001;" aria-hidden="true">
                <div class="modal-dialog">
                    <!-- <form action="{{route('manager.class.add')}}" method="post">-->
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">اضافه کردن دانش آموز</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <div class="mx-auto" style="width: 100%;margin:20px;">
                                        <div class="input-group mb-3">
                                            <!-- /btn-group -->
                                            <input type="hidden" id="id-class">
                                            <input type="text" id="nationalCode" name="national_id" class="form-control"
                                                placeholder="اسم یا کدملی" onkeyup="showResult(this.value)">
                                            <div id="hdResult"></div>
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-danger" onclick="getListofStudents()">اضافه کردن
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body table-responsive p-0">
                                        <table class="table table-hover" id="hdStudentList">
                                            <tbody>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>نام و نام خانوداگی</th>
                                                <th>شماره ملی</th>
                                                <th>عملیات</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>علی علیان</td>
                                                <td>1234567899</td>
                                                <td>
                                                    <button type="button" class="btn btn-danger"><i class="fa fa-trash"
                                                                                                    title="حذف"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>

                                <!-- /.card-body -->

                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>


                            </div>
                        </div>
                    <!--</form>-->

                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>
    <script>

        function getListofStudents(){
            console.log("CLICK");
            var xmlhttp=new XMLHttpRequest();
            
            xmlhttp.onreadystatechange=function() {
                    if (this.readyState==4 && this.status==200) {
                        documnet.getElementByID("hdStudentList").append("");
                    }
                }   
            var StudentID = document.getElementById("nationalCode").value;
            xmlhttp.open("POST","/manager/class/add?student-id=" + StudentID , true);
            xmlhttp.send();
        }
        function getStudent() {
            console.log("CLICK");
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function() {
                    if (this.readyState==4 && this.status==200) {
                        if(this.responseText!=""){

                        }
                    }
                }   
            var StudentID = document.getElementById("nationalCode").innerText.toString();
            xmlhttp.open("POST","/class/get-student?student-id=" + StudentID, true);
            xmlhttp.send();
        }

    /*
        $(document).ready(function () {
            $('body').on('click', '#add-modal', function () {
                document.getElementById('#add-modal').value = $(this).attr('data-class-id')
            });
            $("#hdBTNClass").click(function(){
                console.log("CLICK");
                var xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function() {
                    if (this.readyState==4 && this.status==200) {
                        document.getElementById("hdStudentList").Append=this.responseText;
                    }
                }   
            }
            xmlhttp.open("GET","/class/get-students?classid="+$("#hdClassID").innerhtml(),true);
            xmlhttp.send();
            })
        })
        function showResult(str) {
            if (str.length==0) {
                document.getElementById("hdLiveSearch").innerHTML="";
                document.getElementById("hdLiveSearch").style.border="0px";
                return;
            }
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function() {
                if (this.readyState==4 && this.status==200) {
                document.getElementById("hdLiveSearch").innerHTML=this.responseText;
                document.getElementById("hdLiveSearch").style.border="1px solid #A5ACB2";
                }
            }
            xmlhttp.open("GET","livesearch.php?q="+str,true);
            xmlhttp.send();
            }
    */
    </script>
@endsection
