@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">تعریف پرسنل</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                                    <i class="fa fa-plus"></i>
                                    پرسنل جدید
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست پرسنل</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام</th>
                                            <th>نام خانوادگی</th>
                                            <th>نام پدر</th>
                                            <th>کد ملی</th>
                                            <th>شماره موبایل</th>
                                            <th>سمت</th>
                                            <th>شماره شبا</th>
                                            <th>عملیات</th>
                                        </tr>
                                        @foreach($personals as $personal)
                                        <tr>
                                            <td>{{$personal->user->id}}</td>
                                            <td>{{$personal->user->name}}</td>
                                            <td>{{$personal->user->family}}</td>
                                            <td>{{$personal->father_name}}</td>
                                            <td>{{$personal->national_id}}</td>
                                            <td>{{$personal->user->phone}}</td>
                                            <td>{{$personal->job_title}}</td>
                                            <td>{{$personal->sheba}}</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-edit"
                                               id="edit-modal"
                                               name="edit-modal"
                                                data-personal-id ="{{$personal->user->id}}"
                                                data_name ="{{$personal->user->name}}"
                                                data-family ="{{$personal->user->family}}"
                                                data-father-name = "{{$personal->father_name}}"
                                                data-national-id = "{{$personal->user->national_id}}"
                                                data-phone = "{{$personal->user->phone}}"
                                                data-job-title = "{{$personal->job_title}}"
                                                data-sheba = "{{$personal->sheba}}"

                                                ><i class="fa fa-edit"></i></button>
                                                <form name="delete-form{{$personal->user->id}}" id="delete-form{{$personal->user->id}}" method="post" action="{{route('manager.personal.delete',$personal->user->id)}}">
                                                    @csrf
                                                </form>
                                                <button type="submit" form="delete-form{{$personal->user->id}}" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- modal-default -->
        <div class="modal fade" id="modal-default" style="display: none; z-index: 3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">پرسنل جدید</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('manager.personal.create')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام</label>
                                        <input name="name" type="text" class="form-control form-control-sm" id="inputFirstname" placeholder="نام">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">نام خانوادگی</label>
                                        <input name="family" type="text" class="form-control form-control-sm" id="inputLastname" placeholder="نام خانودگی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام پدر</label>
                                        <input name="father_name" type="text" class="form-control form-control-sm" id="inputFirstname" placeholder="نام پدر">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">کد ملی</label>
                                        <input name="national_id" type="number" class="form-control form-control-sm" id="inputLastname" placeholder="کد ملی">
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <div class="col-sm-6">
                                        <label for="inputFirstname">شماره موبایل</label>
                                        <input name="phone" type="number" class="form-control form-control-sm" id="inputFirstname" placeholder="شماره موبایل">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">سمت</label>
                                        <input name="job_title" type="text" class="form-control form-control-sm" id="inputLastname" placeholder="سمت">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputLastname">شماره شبا</label>
                                        <input name="sheba" type="number" class="form-control form-control-sm" id="inputLastname" placeholder="شماره شبا">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                                <button type="submit" class="btn btn-primary">ثبت</button>

                            </div>
                        </form></div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <div class="modal fade" id="modal-edit" style="display: none; z-index: 3001;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ویرایش پرسنل</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="edit_personal" action="{{route('manager.personal.update')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="inputFirstname">نام</label>
                                    <input name="name" type="text" class="form-control form-control-sm" id="name_edit" placeholder="نام">
                                </div>
                                <div class="col-sm-6">
                                    <label for="inputLastname">نام خانوادگی</label>
                                    <input name="family" type="text" class="form-control form-control-sm" id="family_edit" placeholder="نام خانودگی">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="inputFirstname">نام پدر</label>
                                    <input name="father_name" type="text" class="form-control form-control-sm" id="father_name_edit" placeholder="نام پدر">
                                </div>
                                <div class="col-sm-6">
                                    <label for="inputLastname">کد ملی</label>
                                    <input name="national_id" type="number" class="form-control form-control-sm" id="national_edit" placeholder="کد ملی">
                                </div>
                            </div>
                            <div class="form-group row">

                                <div class="col-sm-6">
                                    <label for="inputFirstname">شماره موبایل</label>
                                    <input name="phone" type="number" class="form-control form-control-sm" id="phone_edit" placeholder="شماره موبایل">
                                </div>
                                <div class="col-sm-6">
                                    <label for="inputLastname">سمت</label>
                                    <input name="job_title" type="text" class="form-control form-control-sm" id="job_title_edit" placeholder="سمت">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="inputLastname">شماره شبا</label>
                                    <input name="sheba" type="number" class="form-control form-control-sm" id="sheba_edit" placeholder="شماره شبا">
                                </div>
                                <div class="col-sm-6">
                                    <input type="hidden" name="id" class="form-control form-control-sm"
                                           id="id_edit">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                            <button type="submit" class="btn btn-primary">ثبت</button>

                        </div>
                    </form></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script defer="" src="/plugins/select2/select2.full.min.js"></script>
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script defer="">
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
    <script>
        $(document).ready(function () {
            $('body').on('click','#edit-modal',function () {
                document.getElementById('name_edit').value = $(this).attr('data_name');
                document.getElementById('family_edit').value = $(this).attr('data-family');
                document.getElementById('father_name_edit').value = $(this).attr('data-father-name');
                document.getElementById('national_edit').value = $(this).attr('data-national-id');
                document.getElementById('phone_edit').value = $(this).attr('data-phone');
                document.getElementById('job_title_edit').value = $(this).attr('data-job-title');
                document.getElementById('sheba_edit').value = $(this).attr('data-sheba')
                document.getElementById('id_edit').value = $(this).attr('data-personal-id');
            })
        })
    </script>
@endsection