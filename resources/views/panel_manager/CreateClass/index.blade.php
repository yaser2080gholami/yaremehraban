@extends('panel.master')
@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    @if($errors->any())
        <h4>{{$errors->first()}}</h4>
    @endif
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">تعریف معلم</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
              </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default" data-toggle="modal"
                                        data-target="#modal-default">
                                    <i class="fa fa-plus"></i>
                                    معلم جدید
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست معلم</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover table-striped ">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>شماره ملی</th>
                                            <th>شماره تماس</th>
                                            <th>عملیات</th>
                                        </tr>
                                        @foreach($teachers as $teacher)
                                            <tr>
                                                <td>{{$teacher->teacher_id}}</td>
                                                <td>{{$teacher->user->name}} {{$teacher->user->family}}</td>
                                                <td>{{$teacher->user->national_id}}</td>
                                                <td>{{$teacher->user->phone}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-primary"
                                                            id="edit-modal"
                                                            name="edit-modal"
                                                            data-teacher-id="{{$teacher->teacher_id}}"
                                                            data-name="{{$teacher->user->name}}"
                                                            data-family="{{$teacher->user->family}}"
                                                            data-father-name="{{$teacher->father_name}}"
                                                            data-national-id="{{$teacher->user->national_id}}"
                                                            data-phone="{{$teacher->user->phone}}"
                                                            data-sheba="{{$teacher->sheba_number}}"
                                                            data-toggle="modal"
                                                            data-target="#modal-edit"><i class="fa fa-edit"></i>
                                                    </button>
                                                    <form name="delete-form{{$teacher->teacher_id}}"
                                                          id="delete-form{{$teacher->teacher_id}}" method="post"
                                                          action="{{route("manager.teacher.delete", $teacher->teacher_id)}}">
                                                        @csrf
                                                    </form>
                                                    <button form="delete-form{{$teacher->teacher_id}}" type="submit"
                                                            class="btn btn-danger"><i
                                                            class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- modal-default -->
        <div class="modal fade" id="modal-default" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">معلم جدید</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="create-teacher" id="create-teacher" method="post" action="{{route('manager.teacher.create')}}">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام</label>
                                        <input type="text" class="form-control form-control-sm" name="name"
                                               id="name" placeholder="نام">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">نام خانوادگی</label>
                                        <input type="text" class="form-control form-control-sm" name="family"
                                               id="family" placeholder="نام خانودگی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام پدر</label>
                                        <input type="text" class="form-control form-control-sm" name="father_name"
                                               id="father-name" placeholder="نام پدر">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">کد ملی</label>
                                        <input type="text" class="form-control form-control-sm" name="national_id"
                                               id="national-id" placeholder="کد ملی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputLastname">شماره تماس</label>
                                        <input type="text" name="phone" class="form-control form-control-sm"
                                               id="phone" placeholder="شماره تماس">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputLastname">شماره شبا</label>
                                    <input type="text" name="sheba" class="form-control form-control-sm"
                                           id="sheba" placeholder="شماره شبا">
                                </div>
                            </div>
                            <!-- /.card-body -->

                        </form>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                            <button form="create-teacher" type="submit" class="btn btn-primary">ثبت</button>
                        </div>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-edit" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ویرایش معلم</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="edit-teacher" id="edit-teacher" method="post" action="{{route('manager.teacher.update')}}">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام</label>
                                        <input type="text" class="form-control form-control-sm" name="name"
                                               id="edit-name" placeholder="نام">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">نام خانوادگی</label>
                                        <input type="text" class="form-control form-control-sm" name="family"
                                               id="edit-family" placeholder="نام خانودگی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام پدر</label>
                                        <input type="text" class="form-control form-control-sm" name="father_name"
                                               id="edit-father-name" placeholder="نام پدر">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">کد ملی</label>
                                        <input type="text" class="form-control form-control-sm" name="national_id"
                                               id="edit-national-id" placeholder="کد ملی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputLastname">شماره تماس</label>
                                        <input type="text" name="phone" class="form-control form-control-sm"
                                               id="edit-phone" placeholder="شماره تماس">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="hidden" name="id" class="form-control form-control-sm"
                                               id="edit-id">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputLastname">شماره شبا</label>
                                    <input type="text" name="sheba" class="form-control form-control-sm"
                                           id="edit-sheba" placeholder="شماره شبا">
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                                <button form="edit-teacher" type="submit" class="btn btn-primary">ثبت</button>
                            </div>
                        </form>

                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('body').on('click', '#edit-modal', function () {
                document.getElementById('edit-name').value = $(this).attr('data-name');
                document.getElementById('edit-family').value = $(this).attr('data-family');
                document.getElementById('edit-father-name').value = $(this).attr('data-father-name');
                document.getElementById('edit-national-id').value = $(this).attr('data-national-id');
                document.getElementById('edit-sheba').value = $(this).attr('data-sheba');
                document.getElementById('edit-phone').value = $(this).attr('data-phone');
                document.getElementById('edit-id').value = $(this).attr('data-teacher-id');
            });
        });
    </script>
@endsection
