@extends('panel.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">تعریف دانش آموز</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="card">
                <!-- <div class="card-header">
                      </div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mx-auto" style="width: 200px;margin:20px;">
                                <button type="button" class="btn btn-default" data-toggle="modal"
                                        data-target="#modal-default">
                                    <i class="fa fa-plus"></i>
                                    دانش آموز جدید
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">لیست دانش آموز</h3>

                                    <div class="card-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="جستجو">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام و نام خانوادگی</th>
                                            <th>شماره ملی</th>
                                            <th>شماره تماس</th>
                                            <th>نام سرپرست</th>
                                            <th>پایه</th>
                                            <th>عملکرد انظباطی</th>
                                            <th>عملکرد تحصیلی</th>
                                            <th>عملیات</th>
                                        </tr>
                                        @foreach($students as $student)
                                            <tr>
                                                <td>{{$student->student_id}}</td>
                                                <td>{{$student->user->name}}  {{$student->user->family}}</td>
                                                <td>{{$student->user->national_id}}</td>
                                                <td>{{$student->user->phone}}</td>
                                                <td>{{$student->parent->name ?? ''}}  {{$student->parent->family ?? ''}}</td>
                                                <td>{{$student->course->name}}</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>
                                                    <button type="button" class="btn btn-success">
                                                        <a href="{{route('manager.student.activate', ['id' => $student->student_id])}}">فعالسازی</a>
                                                    </button>
                                                    <button type="button" class="btn btn-primary"
                                                            id="edit-modal"
                                                            name="edit-modal"
                                                            data-toggle="modal"
                                                            data-student-id="{{$student->student_id ?? ''}}"
                                                            data-father-name="{{$student->parent->name?? ''}}"
                                                            data-parent-national-id="{{$student->parent->national_id?? ''}}"
                                                            data-parent-phone="{{$student->parent->phone?? ''}}"
                                                            data-parent-name="{{$student->parent->name?? ''}}"
                                                            data-parent-family="{{$student->parent->family}}?? ''"
                                                            data-relative="{{$student->relative?? ''}}"
                                                            data-name="{{$student->user->name?? ''}}"
                                                            data-family="{{$student->user->family?? ''}}"
                                                            data-national-id="{{$student->user->national_id?? ''}}"
                                                            data-parent="{{$student->parent->name ?? ''}}"
                                                            data-phone="{{$student->user->phone?? ''}}"
                                                            data-target="#modal-edit"><i class="fa fa-edit"></i>
                                                    </button>
                                                    <form name="delete-form{{$student->student_id}}"
                                                          id="delete-form{{$student->student_id}}" method="post"
                                                          action="{{route("manager.student.delete", $student->student_id)}}">
                                                        @csrf
                                                    </form>
                                                    <button form="delete-form{{$student->student_id}}" type="submit"
                                                            class="btn btn-danger"><i
                                                            class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- modal-default -->
        <div class="modal fade" id="modal-default" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">دانش آموز جدید</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" action="{{route('manager.student.create')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام</label>
                                        <input name="name" type="text" class="form-control form-control-sm"
                                               id="inputFirstname" placeholder="نام">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">نام خانوادگی</label>
                                        <input name="family" type="text" class="form-control form-control-sm"
                                               id="inputLastname" placeholder="نام خانودگی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام پدر</label>
                                        <input name="father_name" type="text" class="form-control form-control-sm"
                                               id="inputFirstname" placeholder="نام پدر">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">کد ملی</label>
                                        <input name="national_id" type="number" class="form-control form-control-sm"
                                               id="inputLastname" placeholder="کد ملی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>دوره و پایه</label>
                                            <select name="course_id" class="form-control select2 form-control-sm"
                                                    style="width: 100%;">
                                                @foreach($courses as $course)
                                                    <option value="{{$course->id}}">{{$course->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">شماره تماس</label>
                                        <input name="phone" type="number" class="form-control form-control-sm"
                                               id="inputLastname" placeholder="شماره تماس">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputLastname">کدملی سرپرست</label>
                                        <input name="parent_national_id" type="number"
                                               class="form-control form-control-sm"
                                               id="parentIDNumber" placeholder="کد ملی سرپرست">
                                    </div>
                                    <div class="col-sm-6" id="parentPhoneNumberDiv">
                                        <label for="parentPhoneNumber">شماره تماس سرپرست</label>
                                        <input name="parent_phone" type="number" class="form-control form-control-sm"
                                               id="parentPhoneNumber" placeholder="شماره تماس سرپرست">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-check">
                                        <input name="same_number" class="form-check-input" type="checkbox"
                                               id="same-number-id">
                                        <label for="same-number-id" class="form-check-label">شماره موبایل اولیا، همان
                                            شماره موبایل دانش آموز
                                            است.</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input name="no_father" class="form-check-input" type="checkbox"
                                               id="seeAnotherFieldGroup">
                                        <label for="seeAnotherFieldGroup" class="form-check-label">پدر سرپرست دانش آموز
                                            نیست.</label>
                                    </div>
                                </div>

                                <!-- /hidden -->
                                <div class="form-group" id="otherFieldGroupDiv">
                                    <fieldset
                                        style="padding-inline-start: 0.75em;padding-inline-end: 0.75em;border:1px solid #ced4da;">
                                        <legend style="width:45%;padding:10px;">مشخصات اولیا</legend>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <label for="inputFirstname">نام</label>
                                                <input name="parent_name" type="text"
                                                       class="form-control form-control-sm" id="inputFirstname"
                                                       placeholder="نام">
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputLastname">نام خانوادگی</label>
                                                <input name="parent_family" type="text"
                                                       class="form-control form-control-sm" id="inputLastname"
                                                       placeholder="نام خانودگی">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>نسبت سرپرست با دانش آموز</label>
                                            <input name="relative" type="text"
                                                   class="form-control form-control-sm" id="relative"
                                                   placeholder="نسبت">
                                        </div>
                                    </fieldset>
                                </div>


                            </div>
                            <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <div class="modal fade" id="modal-edit" style="display: none;z-index:3001;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ویرایش دانش آموز</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="edit-student" id="edit-student" action="{{route('manager.student.update')}}"
                              method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام</label>
                                        <input name="name" type="text" class="form-control form-control-sm"
                                               id="edit-name" placeholder="نام">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">نام خانوادگی</label>
                                        <input name="family" type="text" class="form-control form-control-sm"
                                               id="edit-family" placeholder="نام خانودگی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputFirstname">نام پدر</label>
                                        <input name="father_name" type="text" class="form-control form-control-sm"
                                               id="edit-parent" placeholder="نام پدر">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">کد ملی</label>
                                        <input name="national_id" type="number" class="form-control form-control-sm"
                                               id="edit-national-id" placeholder="کد ملی">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>دوره و پایه</label>
                                            <select name="course_id" class="form-control select2 form-control-sm"
                                                    style="width: 100%;">
                                                @foreach($courses as $course)
                                                    <option value="{{$course->id}}">{{$course->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="inputLastname">شماره تماس</label>
                                        <input name="phone" type="number" class="form-control form-control-sm"
                                               id="edit-phone" placeholder="شماره تماس">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="inputLastname">کدملی سرپرست</label>
                                        <input name="parent_national_id" type="number" class="form-control form-control-sm"
                                               id="edit_parent_national_id" placeholder="کد ملی سرپرست">
                                    </div>
                                    <div class="col-sm-6" id="editParentPhoneNumberDiv">
                                        <label for="editParentPhoneNumber">شماره تماس سرپرست</label>
                                        <input name="parent_phone" type="number" class="form-control form-control-sm"
                                               id="edit_parent_phone" placeholder="شماره تماس سرپرست">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <input type="hidden" name="id" class="form-control form-control-sm"
                                           id="edit-id">
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input name="same_number" class="form-check-input" type="checkbox"
                                               id="edit_same-number">
                                        <label for="edit_same-number" class="form-check-label">شماره موبایل اولیا، همان
                                            شماره موبایل دانش آموز
                                            است.</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input name="no_father" class="form-check-input" type="checkbox"
                                               id="edit_no_father">
                                        <label for="edit_no_father" class="form-check-label">پدر سرپرست دانش آموز
                                            نیست.</label>
                                    </div>
                                </div>

                                <!-- /hidden -->
                                <div class="form-group" id="otherFieldGroupDiv2">
                                    <fieldset
                                        style="padding-inline-start: 0.75em;padding-inline-end: 0.75em;border:1px solid #ced4da;">
                                        <legend style="width:45%;padding:10px;">مشخصات اولیا</legend>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <label for="edit_parent_name">نام</label>
                                                <input name="parent_name" type="text"
                                                       class="form-control form-control-sm" id="edit_parent_name"
                                                       placeholder="نام">
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="edit_parent_family">نام خانوادگی</label>
                                                <input name="parent_family" type="text"
                                                       class="form-control form-control-sm" id="edit_parent_family"
                                                       placeholder="نام خانودگی">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>نسبت سرپرست با دانش آموز</label>
                                            <input name="relative" type="text"
                                                   class="form-control form-control-sm" id="edit_relative"
                                                   placeholder="نسبت">
                                        </div>
                                    </fieldset>
                                </div>


                            </div>
                            <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">لغو</button>
                        <button type="submit" class="btn btn-primary">ثبت</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/plugins/select2/select2.full.min.js"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })

        $("#seeAnotherFieldGroup").change(function () {
            if ($(this).prop('checked') == true) {
                $('#otherFieldGroupDiv').show();
                $('#otherField1').attr('required', '');
                $('#otherField1').attr('data-error', 'This field is required.');
                $('#otherField2').attr('required', '');
                $('#otherField2').attr('data-error', 'This field is required.');
            } else {
                $('#otherFieldGroupDiv').hide();
                $('#otherField1').removeAttr('required');
                $('#otherField1').removeAttr('data-error');
                $('#otherField2').removeAttr('required');
                $('#otherField2').removeAttr('data-error');
            }
        });
        $("#seeAnotherFieldGroup").trigger("change");


        $("#same-number-id").change(function () {
            if ($(this).prop('checked') == true) {
                $('#parentPhoneNumberDiv').hide();
            } else {
                $('#parentPhoneNumberDiv').show();
            }
        });
        $("#same-number-id").trigger("change");

        $("#seeAnotherFieldGroup2").change(function () {
            if ($(this).prop('checked') == true) {
                $('#otherFieldGroupDiv2').show();
                $('#otherField1').attr('required', '');
                $('#otherField1').attr('data-error', 'This field is required.');
                $('#otherField2').attr('required', '');
                $('#otherField2').attr('data-error', 'This field is required.');
            } else {
                $('#otherFieldGroupDiv2').hide();
                $('#otherField1').removeAttr('required');
                $('#otherField1').removeAttr('data-error');
                $('#otherField2').removeAttr('required');
                $('#otherField2').removeAttr('data-error');
            }
        });
        $("#seeAnotherFieldGroup2").trigger("change");


        $("#same-number-id2").change(function () {
            if ($(this).prop('checked') == true) {
                $('#editParentPhoneNumberDiv').hide();
            } else {
                $('#editParentPhoneNumberDiv').show();
            }
        });
        $("#same-number-id2").trigger("change");
    </script>

    <script>
        $(document).ready(function () {
            $('body').on('click', '#edit-modal', function () {
                document.getElementById('edit-name').value = $(this).attr('data-name');
                document.getElementById('edit-family').value = $(this).attr('data-family');
                document.getElementById('edit-phone').value = $(this).attr('data-phone');
                document.getElementById('edit-national-id').value = $(this).attr('data-national-id');
                document.getElementById('edit-parent').value = $(this).attr('data-parent');
                document.getElementById('edit_parent_national_id').value = $(this).attr('data-parent-national-id');
                document.getElementById('edit_parent_phone').value = $(this).attr('data-parent-phone');
                document.getElementById('edit_parent_name').value = $(this).attr('data-parent-name');
                document.getElementById('edit_parent_family').value = $(this).attr('data-parent-family');
                document.getElementById('edit_relative').value = $(this).attr('data-relative');
                document.getElementById('edit-id').value = $(this).attr('data-student-id');
            })
        })
    </script>
@endsection
