AmCharts.translations.dataLoader.fa = {
    'Error loading the file': 'خطا در بارگذاری اطلاعات',
    'Error parsing JSON file': 'خطا در فایل اطلاعات',
    'Unsupported data format': 'نوع فایل پشتیبانی نمی شود',
    'Loading data...': 'درحال بارگذاری...'
}
