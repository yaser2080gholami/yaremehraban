<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'LoginController@logout')->name('logout');
Auth::loginUsingId(10);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('registerSchool', 'registerSchool@index');
Route::post('registerSchool', 'registerSchool@infoSchool')->name('infoSchool');
Route::get('registerStudent', 'registerStudentController@index')->name('registerStudent');
Route::post('registerStudent', 'registerStudentController@postinfo')->name('infoStudent');
Route::get('registerStudent/verify', 'registerStudentController@verify')->name('verify');
Route::post('registerStudent/verify', 'registerStudentController@postverify')->name('postverify');
Route::get('registerStudent/resend-code', 'registerStudentController@resendCode')->name('resend-code');

// Route::middleware('auth')->group(function () {
    Route::get('/', 'Dashboard@index');
    Auth::loginUsingId(1);
    Route::get('/manager', 'DashbordController@index');

    Route::get('dashboard', 'DashbordController@index')->name('dashboard');
    Route::post('/dashboard', 'JobController@postjob')->name('job');
    Route::delete('/dashboard/{id}', 'JobController@deletejob')->name('deleteJob');

//bulletin routes
    Route::group(['as' => 'bulletin', 'prefix' => 'bulletin'], function () {
        Route::get('/', 'BulletinController@index');
        Route::get('/edit', 'BulletinController@edit')->name('.edit');
        Route::post('/store', 'BulletinController@store')->name('.add');
        Route::post('/delete-school/{id}', 'BulletinController@deleteSchool')->name('.delete-school');
        Route::post('/delete-yare-mehraban/{id}', 'BulletinController@deleteMehraban')->name('.delete-mehraban');
    });

//manager routes
    Route::group(['as' => 'manager.', 'prefix' => 'manager'], function () {
        Route::group(['as' => 'teacher.', 'prefix' => 'teacher'], function () {
            Route::get('/', 'ManagerPanelController\TeacherController@index')->name('index');
            Route::post('/create', 'ManagerPanelController\TeacherController@create')->name('create');
            Route::post('/edit', 'ManagerPanelController\TeacherController@update')->name('update');
            Route::post('/delete/{id}', 'ManagerPanelController\TeacherController@delete')->name('delete');
        });
        Route::group(['as' => 'student.', 'prefix' => 'student'], function () {
            Route::get('/', 'ManagerPanelController\StudentController@index')->name('index');
            Route::post('/create', 'ManagerPanelController\StudentController@create')->name('create');
            Route::post('/edit', 'ManagerPanelController\StudentController@update')->name('update');
            Route::post('/delete/{id}', 'ManagerPanelController\StudentController@delete')->name('delete');
            Route::get('/activate/{id}', 'ManagerPanelController\StudentController@activate')->name('activate');
        });
        Route::group(['as' => 'class.', 'prefix' => 'class'], function () {
            Route::get('/', 'ManagerPanelController\ClassController@index')->name('index');
            Route::post('/', 'ManagerPanelController\ClassController@addStudent')->name('add');
            Route::post('/create', 'ManagerPanelController\ClassController@create')->name('create');
            Route::post('/delete', 'ManagerPanelController\ClassController@delete')->name('delete');
            Route::post('/{id}', 'ManagerPanelController\ClassController@showStudent')->name('show');
        });
        Route::group(['as' => 'personal.', 'prefix' => 'personal'], function () {
            Route::get('/', 'ManagerPanelController\PersonalController@index')->name('index');
            Route::post('/', 'ManagerPanelController\PersonalController@create')->name('create');
            Route::post('/edit', 'ManagerPanelController\PersonalController@update')->name('update');
            Route::post('/delete/{id}', 'ManagerPanelController\PersonalController@delete')->name('delete');
        });
        Route::group(['as' => 'task.', 'prefix' => 'task'], function () {
            Route::get('/', 'ManagerPanelController\TaskController@index')->name('index');
            Route::get('/answers', 'ManagerPanelController\TaskController@answers')->name('index-answer');
            Route::get('/download', 'ManagerPanelController\TaskController@downloadFile')->name('download');
        });
    });
    Route::get('manager', 'DashbordController@index');


//student panel
    Route::group(['as' => 'student.', 'prefix' => 'student'], function () {
        Route::group(['as' => 'ticket.', 'prefix' => 'ticket'], function () {
            Route::get('/', 'TicketController@index')->name('index');
            Route::post('/create', 'TicketController@create')->name('create');
            Route::get('send', 'TicketController@sendTicket')->name('send');
        });
        Route::group(['as' => 'calender.', 'prefix' => 'calender'], function () {
            Route::get('/', 'CalenderController@index')->name('index');
        });
        Route::group(['as' => 'online.', 'prefix' => 'online'], function () {
            Route::get('/', 'StudentPanelController\OnlineClassController@index')->name('index');
        });
        Route::group(['as' => 'task.', 'prefix' => 'task'], function () {
            Route::get('/', 'StudentPanelController\TaskController@index')->name('index');
            Route::post('/', 'StudentPanelController\TaskController@answer')->name('answer');
        });
        Route::group(['as' => 'homework.', 'prefix' => 'homework'], function () {
            Route::get('/descriptive', 'StudentPanelController\HomeworkController@indexDescriptive')->name('index_descriptive');
            Route::post('/descriptive', 'StudentPanelController\HomeworkController@answerDescriptive')->name('answer_descriptive');
            Route::get('/test', 'StudentPanelController\HomeworkController@indexTest')->name('index_test');
            Route::post('/test', 'StudentPanelController\HomeworkController@answerTest')->name('answer_test');
        });
        Route::group(['as' => 'report_card.', 'prefix' => 'report_card'], function () {
            Route::get('/', 'StudentPanelController\ReportCardController@index')->name('index');
        });
        Route::group(['as' => 'disciplinary.', 'prefix' => 'disciplinary'], function () {
            Route::get('/', 'StudentPanelController\DisciplinaryController@index')->name('index');
        });
        Route::group(['as' => 'chart.', 'prefix' => 'chart'], function () {
            Route::get('/', 'ChartController@index')->name('index');
        });
    });
    Route::get('student', 'DashbordController@index');

//teacher panel
    Route::group(['as' => 'teacher.', 'prefix' => 'teacher'], function () {
        Route::group(['as' => 'ticket.', 'prefix' => 'ticket'], function () {
            Route::get('/', 'TicketController@index')->name('index');
            Route::post('/create', 'TicketController@create')->name('create');
            Route::get('/send', 'TicketController@sendTicket')->name('send');
        });
        Route::group(['as' => 'calender.', 'prefix' => 'calender'], function () {
            Route::get('/', 'CalenderController@index')->name('index');
        });
        Route::group(['as' => 'online.', 'prefix' => 'online'], function () {
            Route::get('/', 'TeacherPanelController\OnlineClassController@index')->name('index');
            Route::post('/', 'TeacherPanelController\OnlineClassController@create')->name('create');
        });
        Route::group(['as' => 'task.', 'prefix' => 'task'], function () {
            Route::get('/', 'TeacherPanelController\TaskController@index')->name('index');
            Route::post('/create', 'TeacherPanelController\TaskController@create')->name('create');
            Route::post('/update', 'TeacherPanelController\TaskController@update')->name('update');
            Route::post('/delete', 'TeacherPanelController\TaskController@delete')->name('delete');
            Route::get('/correct', 'TeacherPanelController\TaskController@correctTaskIndex')->name('correct-index');
            Route::post('/correct', 'TeacherPanelController\TaskController@correctTask')->name('correct');
            Route::get('/correct/download', 'TeacherPanelController\TaskController@downloadFile')->name('download');
        });
        Route::group(['as' => 'homework.', 'prefix' => 'homework'], function () {
            Route::get('/', 'TeacherPanelController\HomeworkController@index')->name('index');
            Route::post('/create', 'TeacherPanelController\HomeworkController@create')->name('create');
            Route::post('/update', 'TeacherPanelController\HomeworkController@update')->name('update');
            Route::post('/delete', 'TeacherPanelController\HomeworkController@delete')->name('delete');
        });
        Route::group(['as' => 'question.', 'prefix' => 'question'], function () {
            Route::post('/', 'TeacherPanelController\QuestionController@add')->name('add');
        });
        Route::group(['as' => 'report_card.', 'prefix' => 'report_card'], function () {
            Route::get('/', 'TeacherPanelController\ReportCardController@index')->name('index');
        });
        Route::group(['as' => 'disciplinary.', 'prefix' => 'disciplinary'], function () {
            Route::get('/', 'TeacherPanelController\DisciplinaryController@index')->name('index');
            Route::post('/create', 'TeacherPanelController\DisciplinaryController@create')->name('create');
            Route::post('/', 'TeacherPanelController\DisciplinaryController@delete')->name('delete');
        });
        Route::group(['as' => 'chart.', 'prefix' => 'chart'], function () {
            Route::get('/', 'ChartController@index')->name('index');
        });
    });
    Route::get('teacher', 'DashbordController@index');


//parents panel
    Route::group(['as' => 'parents.', 'prefix' => 'parents'], function () {
        Route::group(['as' => 'ticket.', 'prefix' => 'ticket'], function () {
            Route::get('/', 'TicketController@index')->name('index');
            Route::post('/create', 'TicketController@create')->name('create');
            Route::get('send', 'TicketController@sendTicket')->name('send');
        });
        Route::group(['as' => 'calender.', 'prefix' => 'calender'], function () {
            Route::get('/', 'CalenderController@index')->name('index');
        });
        Route::group(['as' => 'task.', 'prefix' => 'task'], function () {
            Route::get('/', 'ParentsPanelController\TaskController@index')->name('index');
            Route::get('/answers', 'ParentsPanelController\TaskController@answers')->name('index-answer');
            Route::get('/download', 'ParentsPanelController\TaskController@downloadFile')->name('download');
        });
        Route::group(['as' => 'homework.', 'prefix' => 'homework'], function () {
            Route::get('/', 'ParentsPanelController\HomeworkController@index')->name('index');
        });
        Route::group(['as' => 'report_card.', 'prefix' => 'report_card'], function () {
            Route::get('/', 'ParentsPanelController\ReportCardController@index')->name('index');
        });
        Route::group(['as' => 'disciplinary.', 'prefix' => 'disciplinary'], function () {
            Route::get('/', 'ParentsPanelController\DisciplinaryController@index')->name('index');
        });
        Route::group(['as' => 'chart.', 'prefix' => 'chart'], function () {
            Route::get('/', 'ChartController@index')->name('index');
        });
    });
    Route::get('parents', 'DashbordController@index');


Route::get('/ticket/get-data', 'AjaxController@getTicketData')->name('gat-data');
Route::get('/class/get-students', 'AjaxController@getStudentsData')->name('gat-students');


